@extends('app')

@section('css')
    <link href={{asset('css/multi-select.css')}} media="screen" rel="stylesheet" type="text/css">
@stop

@section('header')
  Profile Update
@stop

@section('content')
    
<div class="card">
            <div class="card-body">
                <form action="{{route('profile.update', [$role_id, \Auth::user()->id])}}">
                    <div class="container">
                        <div class="row">
                           <h5 class="col-md-2" style="font-family:verdana">Name</h5>   
                           <input class="col-md-8" style="font-family:verdana" type='text' name='name' value='{{$user->name}}'> 
                        </div>
                        
                        <div class="row">
                          <h5 class="col-md-2" style="font-family:verdana">email</h5>
                           <input class="col-md-8" style="font-family:verdana" type='text' name='email' value='{{$user->email}}'> 
                        </div>

                        <div class="row">
                          <h5 class="col-md-2" style="font-family:verdana">Phone No.</h5>
                           <input class="col-md-8" style="font-family:verdana" type='text' name='phone' value='{{$user->phone}}'>
                        </div>
                        <div class="row">
                            <h5 class="col-md-2">Password</h5>    
                            <input class="col-md-8" id="password" type="password" name="password"  autocomplete="new-password">
                        </div>
                        <div class="row">
                            <h5 class="col-md-2">Confirm password</h5>
                            <input class="col-md-8" id="password-confirm" type="password" name="password_confirmation"  autocomplete="new-password">
                        </div>
                        <br class="hidden-xs"/>
                        <button type='submit' class='btn btn-info'>Submit</button>  
                    </div>      
                </form> 
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                       @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                       @endforeach
                    </ul>
                </div>
                @endif
            </div>
</div>
               
            
@stop      

@section('js')

@stop