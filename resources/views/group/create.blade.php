@extends('app')

@section('content')
<div class="card">
  <div class="card-body">
    <div class="row">
        <div class="col-md-6">
            <h3>Add Group name</h3>
            <form action="{{url('groups')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input class="form-control" name="title" required placeholder="Title">
                <br>
                <button class="btn btn-primary" type="submit">Save</button>
            </form>
        </div>
    </div>
  </div>
</div>
@stop