@extends('app')

@section('header')
Groups
@stop

@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

        @if (\Session::has('msg_success'))
        <div class="row">
            <div class="col-lg-10">
                <div class="alert ">
                    <strong>Success! </strong>{{ \Session::get('msg_success') }}
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        @endif

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><a href="{{route('groups.create')}}"><button class="btn btn-primary">Add New</button></a></th>
                    </tr>
                </thead>
            </table>
        </div>

        <div class="table-responsive">
            <table class="table table-hover  mx-auto table-sm">
                <thead class="thead-light">
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Title</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if($groups->count() != 0)
                    @foreach($groups as $val)
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$val->title}}</td>
                        <td>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <form action="{{route('groups.destroy', $val->id)}}" method="post">
                                    @csrf @method('delete')
                                    <button class="btn btn-danger">Delete</button>
                                </form>
                                <form action="{{route('groups.edit', $val->id)}}" method="get">
                                    @csrf
                                    <button class="btn btn-info">Edit</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td>
                            <h5>Please add some groups first</h5>
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
    
@stop