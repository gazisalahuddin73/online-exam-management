@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <h3>Edit Group name</h3>
                    <form action="{{route('groups.update', $data->id)}}" method="post" enctype="multipart/form-data">
                        @csrf @method('PUT')
                        <input class="form-control" name="title" required value="{{old('title', $data->title)}}" placeholder="{{$data->title}}">
                        <br>
                        <button class="btn btn-primary" type="submit">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop