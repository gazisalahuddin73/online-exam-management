@extends('app')

@section('header')
 <h3 style="font-family:verdana">Question List</h3>
@stop

@section('content')
    <div>
        @if(session('status'))
            {{session('status')}}
        @endif
    </div>
    <div class="card">
        <div class="card-body">
            @if (\Session::has('msg_success'))
            <div class="row">
                <div class="col-lg-10">
                    <div  class="alert ">
                        <strong>Success! </strong>{{ \Session::get('msg_success') }}
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>    
            @endif
            <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><a href="{{route('question.create')}}"><button class="btn btn-primary btn-block">Add New</button></a></th>                        
                    </tr>
                </thead>
            </table>
            </div>
            <br>
            
             <div class="table-responsive">
                <table class="table table-hover mx-auto table-sm">
                    <thead class="thead-light">
                        <tr>
                            <th>Question</th>
                            <th>Marks</th>
                            <th>Negative</th>
                            <th>Subject</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                <tbody>
                @if($questions->count() != 0)
                    @foreach($questions as $question)
                        <tr>
                            <td>{!! $question->summernote->instructions !!} </td>
                            <td>{{$question->mark}}</td>
                            <td>{{$question->negative}}</td>
                            <td>{{$question->subject->name}}</td>
                            <td> 
                                <i class="nav-icon fas fa-cogs gear"></i>
                                <div style='display:none' class="btn-group" role="group" aria-label="actions">
                                    <form class="delete" action="{{route('question.destroy', $question->id)}}" method="post">
                                        @csrf @method('delete')
                                        <button class="btn btn-secondary btn-danger">Delete</button>
                                    </form>
                                    <form action="{{route('question.edit', $question->id)}}" method="get">
                                        @csrf
                                        <button class="btn btn-secondary btn-info">Edit</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    @endif
                </tbody>
                </table>
             </div>   
           {!! $questions->render() !!}
          </div>
         </div>
        </div>
    </div>
  @stop

@section('js')
<script>

    $(".delete").on("submit", function(){
        return confirm("Are you sure?");
    });
    $(".gear").on("click", function(){
        $(this).hide()
        $(this).next().removeAttr("style");
    });
</script>
@stop
