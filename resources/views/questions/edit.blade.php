@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
        @if(\Auth()->user()->hasAnyDirectPermission(['edit question']))
            <form id="exam" method="post" action="{{route('question.update', $question->id)}}">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <h1>Add New Question</h1>
                        <textarea id="summernote" name="body" required>{!! $question->summernote->instructions !!}</textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <h5>Subject</h5>
                        <select title="subject" name="subject_id">
                            @foreach($subjects as $subject)
                                @if($subject_name === $subject->name )
                                    @php $status = 'selected';  @endphp
                                  @else
                                    @php  $status = '';  @endphp
                                @endif
                                <option {{$status}} value="{{$subject->id}}">{{$subject->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <h5>Question Marks</h5>
                        <input name="mark" placeholder="Question Marks" value="{{$question->mark}}">
                    </div>
                    <div class="col-sm-3">
                        <h5>Negative Marks</h5>
                        <input value=0 name="negative" placeholder="Negative Marks">
                    </div>
                    <div class="col-sm-3">
                        <h5>Question order</h5>
                        <input value=0 name="order" placeholder="Question order">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <strong>Options</strong>
                        <select id="optionsCount">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                </div>
                <div id="optionList" class="col-sm-12">
                    @foreach($options as $key=>$value)
                        @php $name = 'correct'; @endphp
                        @if($key == $correct[0])
                           @php $checked = 'checked'; @endphp
                          @else
                           @php $checked = ''; @endphp
                        @endif
                        <input class="col-sm-8" type="text" name="{{'option_'.($key+1)}}" value="{{$value}}"><strong class="col-sm-2">  is correct</strong> <input {{$checked}} class="input_type col-sm-1" type=radio value="{{'option_'.($key+1)}}" name="{{$name}}">
                    @endforeach
                </div>
                <br>
                <button class="btn btn-info" id="save" type="submit" >Update</button>
            </form>
        @endif     
        </div>
    </div>
@stop

@section('css')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
@stop

@section('js')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 300,
                width: 1100,
                popover: {
                    image: [],
                    link: [],
                    air: []
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            //create input element for nr optionList
            $("#optionsCount").change(function() {
                var value = $(this).val();
                var nr = 0;
                var elem = $('#optionList').empty();

                while (nr < value) {
                        elem.append('<input class="col-sm-8" name="'+`option_${nr+1}`+'">'+'<strong class="col-sm-2">  is correct</strong>'+'<input class="input_type col-sm-1" type="radio" value="'+`option_${nr+1}`+'" name="correct">');
                        nr++;
                }

            });
        });
    </script>
@stop
