@extends('app')

@section('content')
    <div class="card">
        @if (\Session::has('msg_success'))
        <div class="row">
                <div class="col-lg-10">
                    <div  class="alert ">
                        <strong>Success! </strong>{{ \Session::get('msg_success') }}
                    </div>
                </div>
                <div class="clearfix"></div>
        </div>    
        @endif
       
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><a href="{{route('subjects.create')}}"><button class="btn btn-primary">Add New</button></a></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="table-responsive">
                   <table class="table table-hover  mx-auto table-sm">
                       <thead>
                           <tr>
                               <th></th>
                               <th></th>
                               <th></th>
                               <th></th>
                               <th>Name</th>
                               <th>Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                       @if($subjects->count() != 0)
                           @foreach($subjects as $subject)
                               <tr>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td>{{$subject->name}}</td>
                                   <td>
                                       <div class="btn-group" role="group" aria-label="Basic example">
                                           <form action="{{route('subjects.destroy', $subject->id)}}" method="post">
                                               @csrf @method('delete')
                                               <button class="btn btn-danger">Delete</button>
                                           </form>
                                           <form action="{{route('subjects.edit', $subject->id)}}" method="get">
                                               @csrf
                                               <button class="btn btn-info">Edit</button>
                                           </form>
                                       </div>
                                   </td>
                               </tr>
                           @endforeach
                       @else
                           <tr>
                               <td><h5>Please add some subjects first</h5></td>
                           </tr>
                       @endif
                       </tbody>
                   </table>
            </div>
        </div>
    </div>

@stop
