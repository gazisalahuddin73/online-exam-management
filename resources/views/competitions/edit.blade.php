@extends('app')

@section('css')
    <link href={{asset('css/multi-select.css')}} media="screen" rel="stylesheet" type="text/css">
@stop
@section('content')
    <div class="card">
        <div class="card-body" id="main">
           <div class="row">
            <div class="col-md-4"> <h3>Enter competition title </h3> </div>
            <div class="col-md-8">  
             <input type="text" id="title" placeholder="Competition title" value="{{$competition->title}}" required>
            </div>
           </div>
            <hr>
            <div class="row">
                <h4 class="col-md-2">Select Exams</h4>
                <select multiple="multiple" id="exam_ids" name="exam_ids[]">
                @foreach($exams as $exam)
                    <option value="{{$exam->id}}">{{$exam->name}}</option>
                @endforeach
                </select>
            </div>
            <br>
            <div class="row">
                <h4 class="col-md-2">Select Institution</h4>
                <select multiple="multiple" name="institution_ids[]" id="institution">
                    @foreach($institutions as $institution)
                        <option value={{$institution->id}}>{{$institution->name}}</option>
                    @endforeach
                </select>
            </div>
            <br>

            <div class="row">
                <h4 class="col-md-2">Select Class</h4>
                <select multiple="multiple" id="class" name="class[]">
                    @for($i=1; $i<=10; $i++)
                        <option value={{$i}}>{{$i}}</option>
                    @endfor
                </select>
            </div>

            <br>
            <div id="student">

            </div>

            <button id="submit" type="submit" class="btn btn-info">Update</button>

        </div>
    </div>
@stop

@section('js')
    <script src="{{asset('js/jquery.multi-select.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#exam_ids').multiSelect()
            $('#institution').multiSelect()
            $('#class').multiSelect()
        })
    </script>


    {{--<script type="text/javascript">--}}
    {{--    $(document).ready(function(){--}}
    {{--        $('.class').click(function(e){--}}
    {{--                e.preventDefault();--}}
    {{--                let institution = $('#institution').val();--}}
    {{--                $.ajax({--}}
    {{--                    type:'POST',--}}
    {{--                    headers: {--}}
    {{--                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
    {{--                    },--}}
    {{--                    url:'{{url('')}}'+'/competitions/student/list/from/institution/'+`${institution}`+'?class='+$(this).val(),--}}
    {{--                    data:{},--}}
    {{--                    success:function(data) {--}}
    {{--                        $('#student_ids').remove()--}}
    {{--                        $('#ms-student_ids').remove()--}}
    {{--                        $('#main').append(data.main_select_element);--}}
    {{--                        $('#student_ids').after(data.two_visible_divs)--}}
    {{--                    }--}}
    {{--                })--}}
    {{--            }--}}
    {{--        )--}}
    {{--    })--}}
    {{--</script>--}}

    <script type="text/javascript">
        $(document).ready(function(){
            $('#submit').click(function(e){
                    e.preventDefault();

                    let institution_id = $('#institution').val();

                    let institution_ids = [];
                    $.each($("#institution option:selected"), function(){
                        institution_ids.push($(this).val());
                    });

                    let exam_ids = [];
                    $.each($("#exam_ids option:selected"), function(){
                        exam_ids.push($(this).val());
                    });


                    let class_selected = [];
                    let y;
                    for (y of $("#ms-class .ms-elem-selection.ms-selected")) {
                        class_selected.push($(y).find("span").text())
                    }

                    $.ajax({
                        type:'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:'{{route('competition.update', $competition->id)}}',
                        data:{
                            exam_ids,
                            class_selected,
                            institution_ids,
                            title: $('#title').val()
                        },
                        success:function(data) {
                            window.location='{{url('competitions')}}'
                        }
                    })
                }
            )
        })
    </script>

    {{--<script type="text/javascript">--}}
    {{--    $(document).ready(function(){--}}
    {{--        $('#student_ids').multiSelect()--}}
    {{--    })--}}
    {{--</script>--}}
@stop
