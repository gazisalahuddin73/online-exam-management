@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover  mx-auto table-sm table-striped">
                <thead class="thead-dark">
                        <tr>
                            <th>Name</th>
                            <th>School</th>
                            <th>Roll No.</th>
                            <th>Obtained Marks</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($final as $value )
                            @php $student = \App\User::find($value['student_id']) @endphp
                            <tr>
                                <td>{{$student->name}}</td>
                                <td>{{$student->institution->name}}</td>
                                <td>{{$student->roll_no}}</td>
                                <td>{{$value['mark']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
