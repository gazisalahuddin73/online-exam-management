@extends('app')

@section('header')
 Competitions
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            @if (\Session::has('msg_success'))
            <div class="row">
                <div class="col-lg-10">
                    <div  class="alert ">
                        <strong>Success! </strong>{{ \Session::get('msg_success') }}
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>    
            @endif
            <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><a href="{{route('competitions.create')}}"><button class="btn btn-primary">Add New</button></a></th>
                    </tr>
                </thead>
            </table>
        </div>

        <div class="table-responsive">
            <table class="table table-hover  mx-auto table-sm">
                <thead class="thead-light">
                         <tr>
                             <th></th>
                             <th></th>
                             <th></th>
                             <th></th>
                             <th>Title</th>
                             <th>Action</th>
                         </tr>
                        </thead>
                        <tbody>
                         @foreach($competitions as $competition )
                                 <tr>
                                     <td></td>
                                     <td></td>
                                     <td></td>
                                     <td></td>
                                     <td>{{$competition->title}}</td>
                                     <td>
                                     <div class="modal" tabindex="-1" role="dialog" id="link{{$competition->id}}">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Modal title</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <input type="textbox" value="{{url('enroll/competition')}}/{{$competition->title}}">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                         <i class="nav-icon fas fa-cogs gear"></i>
                                         <div style='display:none' class="btn-group" role="group" aria-label="">
                                             <a href="{{route('competition.show', $competition->id)}}"><button class="btn btn-info">View</button></a>
                                             <a href="{{route('competition.edit', $competition->id)}}"><button class="btn btn-primary">Edit</button></a>
                                             <button class="btn btn-info" data-toggle="modal" data-target="#link{{$competition->id}}">Link</button>
                                             <form class='delete' action={{route('competition.delete', $competition->id)}} method="post">
                                                @csrf @method('delete')
                                                <button class="btn btn-danger" type='submit'>Delete</button>
                                            </form>
                                         </div>
                                     </td>
                                 </tr>
                         @endforeach
                         </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
<script>
    $(".gear").on("click", function(){
        $(this).hide()
        $(this).next().removeAttr("style");
    });

    $(".delete").on("submit", function(){
        return confirm("Are you sure?");
    });
</script>
@stop
