@extends('app')

@section('header')
Institutions
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            @if (\Session::has('msg_success'))
            <div class="row">
                <div class="col-lg-10">
                    <div  class="alert ">
                        <strong>Success! </strong>{{ \Session::get('msg_success') }}
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>    
            @endif
            <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><a href="{{route('institutions.create')}}"><button class="btn btn-primary">Add New</button></a></th>
                    </tr>
                </thead>
            </table>
        </div>
               <div class="table-responsive">
                   <table class="table table-hover  mx-auto table-sm">
                       <thead class="thead-light">
                           <tr> 
                               <th></th>
                               <th></th>
                               <th></th>
                               <th></th>
                               <th></th>
                               <th></th>
                               <th>Name</th>
                               <th>Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                       @if($institutions->count() != 0)
                           @foreach($institutions as $institution)
                               <tr>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td>{{$institution->name}}</td>
                                   <td>

                                       <div class="btn-group" role="group" aria-label="Basic example">
                                           <form action="{{route('institutions.destroy', $institution->id)}}" method="post">
                                               @csrf @method('delete')
                                               <button class="btn btn-danger">Delete</button>
                                           </form>
                                           <form action="{{route('institutions.edit', $institution->id)}}" method="get">
                                               @csrf
                                               <button class="btn btn-info">Edit</button>
                                           </form>
                                       </div>

                                   </td>
                               </tr>
                           @endforeach
                       @else
                           <tr>
                               <td><h1>Please add some institutions first</h1></td>
                           </tr>
                       @endif
                       </tbody>
                   </table>
               </div>
            
        </div>
    </div>

@stop
