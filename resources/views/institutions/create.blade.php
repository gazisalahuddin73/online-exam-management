@extends('app')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
                <div class="col-md-6">
                    <h3 style="font-family:verdana">Add Institution Name</h3>
                    <form action="{{route('institutions.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input class="form-control" name="name" required placeholder="Title">
                        <br>
                        <button class="btn btn-primary" type="submit">Save</button>
                    </form>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
        </div>
    </div>
</div>
@stop