@extends('app')

@section('content')
<div class="card">
    <div class="card-body">
    <div class="row">
        <div class="col-md-6">
            <h3 style="font-family:verdana">Edit Institution</h3>
            <form action="{{route('institutions.update', $institution->id)}}" method="post" enctype="multipart/form-data">
                @csrf @method('PUT')
                <input class="form-control" name="name" value="{{$institution->name}}" required placeholder="Title">
                <br>
                <button class="btn btn-primary" type="submit">Save</button>
            </form>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
    </div>
</div>
@stop