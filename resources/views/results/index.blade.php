@extends('app')

@section('header')
 Results
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
        <div class="table-responsive">
                <table class="table table-hover  mx-auto table-sm">
                    <thead class="thead-light">
                      <tr>
                          <th>Exams</th>
                          <th>No. of Students</th>
                          <th>Results</th>
                          <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($results as $result)
                     <tr>
                         <td>{{$result->name}}</td>
                         <td>{{$result->students}}</td>
                         <td><a href="{{route('result.teacher', [$result->exam_id])}}"><button>View</button></a></td>
                         <td>
                             <div class="btn-group" role="group" aria-label="Basic example">
                                 <a><button type="button" class="btn btn-secondary">Delete</button></a>
                             </div>
                         </td>
                     </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
