<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Online Exam Management') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<main class="py-4">    
<div class="container">    
    <div class="card">
        <div class="card-body">
            <h4>Exam: {{request()->examName}}</h4>  
            <h6>(Please put your login credentials to be enrolled to this exam)</h6>
             
            <form action="{{route('enroll.student.frontend')}}" method="POST">
                @csrf 
                    <input type="hidden" name="exam" value="{{request()->examName}}">
                    <div class="row">
                        <div class="col-md-2">
                            email/Phone No.
                        </div>
                        <div class="col-md-6">
                            <input name="email" type="text" placeholder="email/phone">  
                        </div>                                                  
                    </div>
                            
                    <br class="hidden-xs"/>
                            
                    <div class="row">
                        <div class="col-md-2">
                             Password
                        </div>
                        <div class="col-md-6">
                         <input id="password" type="password" class="@error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        </div>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror                                                  
                    </div>
                    
                    <br class="hidden-xs"/>	
                            
                <button type="submit" name="submit" class="btn btn-info">Enroll</button>
            </form>

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
          
        </div>
    </div>                                
</div>
</main> 
</body>
</html>