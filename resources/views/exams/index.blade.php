@extends('app')

@section('header')
<h4 style="font-family:verdana">List of Exams</h4> 
@endsection

@section('content') 
    <div class="card">
        <br>
        <div class="row"> 
            <div class="col-md-9"></div>
            <div class="col-md-2">
             <a href="{{route('exams.create')}}"><button class="btn btn-primary btn-block">Add New</button></a>
            </div>
            <div class="col-md-1"></div>
        </div>
         <div class="card-body">
           @if (\Session::has('msg_success'))
                    <div class="row">
                        <div class="col-lg-10">
                            <div  class="alert ">
                                <strong>Success! </strong>{{ \Session::get('msg_success') }}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>    
            @endif
           
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-hover  mx-auto table-sm">
                            <thead thead-light>
                                <tr>
                                    <th>Exam</th>
                                    <th>Group</th>
                                    <th>Exam Date Time</th>
                                    <th>Duration(minute)</th>
                                    <th>Questions</th>
                                    <th>Marks</th>
                                    <th>Status</th>
                                    <th>Questions</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody id="tbody">
                            @if($exams->count() != 0)
                                @foreach($exams as $val)
                                    <tr>
                                        <td>{{$val->name}}</td>
                                        <td>{{$val->group->title}}</td>
                                        <td>{{$val->date_time}}</td>
                                        <td>{{$val->duration}}</td>
                                        <td>{{$val->questions->count()}}</td>
                                        <td>{{$val->questions->sum('mark')}}</td>
                                        <td>@if(\App\Result::where('exam_id', $val->id)->exists())
                                                Completed
                                            @else
                                                Pending
                                            @endif
                                        </td>
                                        <td>
                                            <div class="modal" tabindex="-1" role="dialog" id="myModal{{$val->id}}">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Modal title</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="{{route('select.question.set', $val->id)}}" method="post">
                                                                @csrf
                                                                <select name="bank_id">
                                                                    @foreach(\App\Bank::get() as $bank)
                                                                    <option value={{$bank->id}}>{{$bank->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <button type="submit">Assign</button>
                                                            </form>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <i class="nav-icon fas fa-cogs gear1"></i>
                                            <div class="btn-group" style='display:none' class="btn-group" role="group" aria-label=""> 
                                                <button class="btn btn-info" type="button" data-toggle="modal" data-target="#myModal{{$val->id}}">Set</button>
                                                
                                                <form action="{{route('exams.manage', $val->id)}}" method="get">
                                                    @csrf
                                                    <button class="btn btn-primary">Manage</button>
                                                </form>
                                            </div> 
                                        </td>
                                        <td>
                                            <div class="modal" tabindex="-1" role="dialog" id="link{{$val->id}}">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Modal title</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <input type="textbox" value="{{url('enroll/exam')}}/{{$val->name}}">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <i class="nav-icon fas fa-cogs gear2"></i>
                                            <div class="btn-group" style='display:none' role="group" aria-label="">
                                                    <form class='delete' action="{{route('exams.destroy', $val->id)}}" method="post">
                                                        @csrf @method('delete')
                                                        <button class="btn btn-danger" type='submit'>Delete</button>
                                                    </form>

                                                    <form action="{{route('exams.edit', $val->id)}}" method="get">
                                                        @csrf
                                                        <button class="btn btn-primary">Edit</button>
                                                    </form>
                                            
                                                    <button class="btn btn-info" data-toggle="modal" data-target="#link{{$val->id}}">Link</button>
                                                
                                                    @if($val->activate_exam == 1)
                                                       <button value="{{$val->id}}" class="btn btn-danger activate">Deactivate</button>  
                                                     @else
                                                       <button value="{{$val->id}}" class="btn btn-info activate">Activate</button> 
                                                    @endif  
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <h1>Please add some exams first</h1>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
<script type="text/javascript">
        $(document).ready(function(){
            $('.activate').click(function(e){
                    e.preventDefault();
                    if ($(this).text().trim()==='Deactivate'){
                        $(this).text('Activate')
                        $(this).removeClass('activate btn btn-danger')
                        $(this).addClass('activate btn btn-info')
                    }else if ($(this).text().trim()==='Activate'){
                        $(this).text('Deactivate')
                        $(this).removeClass('activate btn btn-info')
                        $(this).addClass('activate btn btn-danger')
                    }
                    $.ajax({
                        type:'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:'{{url('')}}'+'/exams/activation/'+$(this).val(),
                        data:'_token = <?php echo csrf_token() ?>',
                        success:function(data) {
                            
                        }
                    })
                }
            );
        });
</script>
<script>
    $(".gear1").on("click", function(){
        $(this).hide()
        $(this).next().removeAttr("style");
    });

    $(".gear2").on("click", function(){
        $(this).hide()
        $(this).next().removeAttr("style");
    });
</script>
<script>
    $(".delete").on("submit", function(){
        return confirm("Are you sure?");
    });
</script>
@stop
