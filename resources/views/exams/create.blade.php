@extends('app')

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{route('exams.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <h4 class="col-md-2" style="font-family:verdana">Exam Name</h4>
                            <input required class="col-md-6" name="name" class="form-control">
                        </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-2" style="font-family:verdana">Group Name:</h4>
                            <select class="col-md-6" title="group" name="group_id">
                                @foreach($groups as  $group)
                                    <option value="{{$group->id}}"> {{$group->title}} </option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        <div class="row">
                           <h4 class="col-md-2" style="font-family:verdana">Date and Time:</h4>
                            <input required class="col-md-6 form-control" type="datetime-local" name="date_time">
                        </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-2" style="font-family:verdana">Exam Duration <span> (in minutes) </span>:</h4>
                            <input required class="col-md-6" type="number" name="duration" placeholder="duration">
                        </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-8" style="font-family:verdana">Instructions:</h4>
                            <textarea id="summernote" class="form-control summernote" name="instructions"></textarea>
                        </div>
                        <br>
                        <button class="btn btn-primary" type="submit">Save</button>
                    </form>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </div>

@stop

@section('css')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
@stop

@section('js')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height:100,
                width:1100,
                popover: {
                    image: [],
                    link: [],
                    air: []
                }
            });
        });
    </script>
@stop
