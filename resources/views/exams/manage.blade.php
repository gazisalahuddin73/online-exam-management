@extends('app')

@section('content')
<div class="card">
 <div class="card-body">
   <div class="card">
        <div class="card-body">
            <div class="row">
                  <div class="col-md-12">
                      <h3>Exam: {{$exam->name}} / Group: {{$group}}/ Duration: {{$exam->duration}} min / Date: {{$exam->date_time}}</h3>
                  </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <select title="subject" name="subject_id" id="select">
                <option disabled selected>Select Subject</option>
                {!! $options !!}
            </select>
            <input name="question_description" id="question_description" placeholder="enter text to search questions">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th>Question</th>
                        <th>Marks</th>
                        <th>Negative</th>
                        <th>Subject</th>
                        <th>Select</th>
                    </tr>
                </thead>
                <tbody id="question_selector">
                </tbody>
            </table>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <h5>Selected Questions List</h5>
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>Question</th>
                            <th>Marks</th>
                            <th>Negative</th>
                            <th>Subject</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody id="tbody">
                    @if($questions)
                        @foreach($questions as $question)
                            <tr id="_{{$question->id}}">
                                <td>{!!$question->summernote->instructions!!}</td>
                                <td>{{$question->mark}}</td>
                                <td>{{$question->negative}}</td>
                                <td>{{$question->subject->name}}</td>                           
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <button value="{{$question->id}}" class="unset btn btn-danger">Unset</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td><h1>Please add some questions first</h1></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>   
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <form id="question" method="post" action="{{route('exam.manage.question.save')}}">
                <input type="hidden" name="exam_id" value="{{$exam->id}}">
                @csrf
                 
                        <textarea id="summernote" name="body" required></textarea>
                   
                <div class="row">
                    <div class="col-sm-3">
                        <h5>Subjects</h5>
                        <select title="subject" name="subject_id">
                            <option selected disabled>select</option>
                            @foreach($subjects as $subject)
                                <option value="{{$subject->id}}">{{$subject->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-3">
                        <h5>Question Marks</h5>
                        <input name="mark" placeholder="Question Marks">
                    </div>
                    <div class="col-sm-3">
                        <h5>Negative Marks</h5>
                        <input value=0 name="negative" placeholder="Negative Marks">
                    </div>
                    <div class="col-sm-3">
                        <h5>Question order</h5>
                        <input value=0 name="order" placeholder="Question order">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <strong>Options</strong>
                        <select id="optionsCount">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>

                    <div id="optionList" class="col-sm-12">
                        <input class="col-sm-8" type="text" name="option_1"><strong class="col-sm-2">  is correct</strong> <input class="input_type col-sm-1" type="radio" value="option_0" name="correct">
                        <input class="col-sm-8" type="text" name="option_2"><strong class="col-sm-2">  is correct</strong> <input class="input_type col-sm-1" type="radio" value="option_1" name="correct">
                        <input class="col-sm-8" type="text" name="option_3"><strong class="col-sm-2">  is correct</strong> <input class="input_type col-sm-1" type="radio" value="option_2" name="correct">
                        <input class="col-sm-8" type="text" name="option_4"><strong class="col-sm-2">  is correct</strong> <input class="input_type col-sm-1" type="radio" value="option_3" name="correct">
                    </div>
                </div>
                <br>
                <button id="save" type="submit" class="btn btn-info btn-block">Save</button>
            </form>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
 </div>
</div>
@stop

@section('css')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
@stop

@section('js')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 200,
                width: 1000,
                popover: {
                    image: [],
                    link: [],
                    air: []
                }
            });
        });
    </script>
    <script type="text/javascript">
            $(document).ready(function(){
                //$('.unset').click(function(){
                    $(document).on("click", ".unset", function(){
                    let id = $(this).val();
                        $.ajax({
                            type:'POST',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url:'{{url('')}}'+'/question/unset/'+$(this).val(),
                            data:{exam_id: "{{$exam->id}}" },
                            success:function(data) {
                               // $("#_"+id).hide()
                               $("#tbody").html(data.message);
                            }
                        })
                    }
                );
            });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#select').change(function(){
                $.ajax({
                    type:'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'{{url('')}}'+'/question/select/'+$(this).val(),
                    data:{exam_id: "{{$exam->id}}" },
                    success:function(data) {
                        $("#question_selector").html(data.message);
                    }
                })
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#question_selector').change(function(){
                $.ajax({
                    type:'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'{{url('')}}'+'/question/set/'+$('input[name="question_id"]:checked').val(),
                    data:{exam_id: "{{$exam->id}}" },
                    success:function(data) {
                        $('#select').val('select').prop('selected', true);
                        $("#question_selector").html(data.message);
                        $("#tbody").html(data.list);
                    }
                })
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            //create input element for nr optionList
            $("#optionsCount").change(function() {
                var value = $(this).val();
                var nr = 0;
                var elem = $('#optionList').empty();

                while (nr < value) {
                    elem.append('<input class="col-sm-8" name="'+`option_${nr+1}`+'">'+'<strong class="col-sm-2">  is correct</strong>'+'<input class="input_type col-sm-1" type="radio" value="'+`option_${nr}`+'" name="correct">');
                    nr++;
                }

            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#question_description').keyup(function(){
                $.ajax({
                    type:'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'{{url('')}}/exams/manage/question/list/'+$('#select').val(),
                    data:{
                        question_description: $('#question_description').val(),
                        exam_id: "{{$exam->id}}"
                    },
                    success:function(data) {
                        $("#question_selector").html(data.message);
                    }
                })
            });
        });
    </script>
@stop
