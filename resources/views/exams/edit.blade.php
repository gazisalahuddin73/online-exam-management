@extends('app')

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{route('exams.update', $exam->id)}}" method="post" enctype="multipart/form-data">
                        @csrf @method('PUT')
                        <div class="row">
                            <h4 class="col-md-2" style="font-family:verdana">Exam name</h4>
                            <input required class="col-md-6" name="name" class="form-control" value="{{$exam->name}}">
                        </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-2" style="font-family:verdana">Group Name:</h4>
                            <select class="col-md-6" title="group" name="group_id">
                                @foreach($groups as  $group)
                                    <option value="{{$group->id}}"> {{$group->title}} </option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-2" style="font-family:verdana">Date and Time:</h4>
                            <input required class="col-md-6 form-control" type="datetime-local" name="date_time" value="{{$exam->date_time->toDateTimeLocalString()}}">
                        </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-2" style="font-family:verdana">Exam Duration (in minutes):</h4>
                            <input required class="col-md-6" type="number" name="duration" placeholder="duration" value="{{$exam->duration}}">
                        </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-8" style="font-family:verdana">Instructions:</h4>
                            <textarea id="summernote" class="form-control summernote" name="instructions">{!! $exam->load('summernote')->summernote->instructions !!}</textarea>
                        </div>
                        <br>
                        <button class="btn btn-primary" type="submit">Update</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
@stop

@section('css')
    <style>
        .note-insert {
            display: none
        }

        .star:after {
            content:"*";
            color:red;
        }
    </style>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
@stop

@section('js')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height:100,
                width:1100,
                popover: {
                    image: [],
                    link: [],
                    air: []
                }
            });
        });
    </script>
@stop
