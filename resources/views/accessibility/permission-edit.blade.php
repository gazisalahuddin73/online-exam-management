@extends('app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Accessibility Control Center') }}</div>
                        <div class="card-body">
                         <form action={{route('update.permission', $permission->id)}}>
                            @csrf
                            
                            <input value='{{$permission->name}}' required name='name'>
                            <button type='submit' class='btn btn-info'>Update</button>
                         </form>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop                            