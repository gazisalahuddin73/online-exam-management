@extends('app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                @if (\Session::has('msg_success'))
                <div class="row">
                    <div class="col-lg-10">
                        <div  class="alert ">
                            <strong>Success! </strong>{{ \Session::get('msg_success') }}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>    
                @endif
                    <div class="card-header">{{ __('Accessibility Control Center') }}</div>
                        <div class="card-body">
                         <div class="table-responsive">  
                            <table class="table table-hover  mx-auto table-sm">
                              <thead class="thead-light">
                              <tr>
                                <th>
                                Role
                                </th>
                                <th>
                                Permissions
                                </th>
                                <th>
                                Actions
                                </th>
                              </tr>
                              </thead>
                              <tbody>
                              @foreach($final as $key=>$value)
                              <tr>
                                <td>
                                  @php 
                                    echo \Spatie\Permission\Models\Role::find($key+1)->name; 
                                  @endphp
                                </td>
                                <td>
                                  <ul>
                                  @foreach($value as $collection) 
                                      @foreach($collection as $item)
                                      @foreach($item as $permission)
                                          @php echo  '<li>'; @endphp
                                            @php 
                                              echo \Spatie\Permission\Models\Permission::find($permission->permission_id)->name; 
                                            @endphp
                                          @php echo  '</li>'; @endphp
                                        @endforeach
                                      @endforeach
                                  @endforeach
                                  </ul>
                                </td>
                                <td>
                                <a href="{{route('accessibility.edit', $key+1)}}">Edit</a>
                                </td>
                              </tr>
                              @endforeach
                              </tbody>
                            </table>
                         </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop                    
