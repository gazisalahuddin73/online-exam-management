@extends('app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Accessibility Control Center: Edit') }}</div>
                    
                        <div class="row">
                         <div class='col-md-2'></div>   
                         <div class='col-md-6'>
                            <h3>Role: {{$role->name}} </h3>
                            <select multiple="multiple" id="permission_ids" name="exam_ids[]">
                                @foreach($permissions as $permission)
                                    <option value="{{\Spatie\Permission\Models\Permission::find($permission)->id}}">{{\Spatie\Permission\Models\Permission::find($permission)->name}}</option>
                                @endforeach
                            </select>
                            <br>
                            <button type="submit" id="submit" class="btn btn-info">Submit</button>
                         </div>
                         <div class='col-md-4'></div> 
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop      

@section('js')
<script src="{{asset('js/jquery.multi-select.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#permission_ids').multiSelect() 
    })
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#submit').click(function(e){
                e.preventDefault();

                let permission_ids = [];
                $.each($("#permission_ids option:selected"), function(){
                    permission_ids.push($(this).val());
                });

                $.ajax({
                    type:'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'{{route('accessibility.update', $role->id)}}',
                    data:{
                        permission_ids
                    },
                    success:function(data) {
                        window.location='{{route('accessibility')}}';
                    }
                })
            }
        )
    })
</script>
@stop