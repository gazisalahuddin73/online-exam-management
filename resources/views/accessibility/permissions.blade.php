@extends('app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Accebility Control Center') }}</div>
                        <div class="card-body">
                            @if (\Session::has('msg_success'))
                            <div class="row">
                                    <div class="col-lg-10">
                                        <div  class="alert ">
                                            <strong>Success! </strong>{{ \Session::get('msg_success') }}
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                            </div>    
                            @endif
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th><a href="{{route('add.permission')}}"><button class="btn btn-primary">Add Permission</button></a></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>

                            <div class="table-responsive">
                            <table class="table table-hover  mx-auto table-sm">
                             <thead class="thead-light">
                                        <tr>
                                            <th>
                                                Permission
                                            </th>
                                            <th>
                                                Action
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($permissions as $permission)
                                        <tr>
                                            <td>
                                            {{$permission->name}}
                                            </td>
                                           
                                            <td>
                                                <a href={{route('edit.permission', $permission->id)}}><button class='btn btn-info'> Edit </button><a>
                                                <a href='{{route('delete.permission', $permission->id)}}'><button class='btn btn-danger'> Delete </button><a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $permissions->render() !!}
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop        