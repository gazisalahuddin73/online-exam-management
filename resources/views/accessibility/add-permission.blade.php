@extends('app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Accebility Control Center') }}</div>
                        <div class="card-body">
                         <form action={{route('save.permission')}}>
                            <input required type='text' name="name" placeholder='enter permission name here'>
                            
                            <button type="submit" class="btn btn-info">Add</button>
                         </form>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop                            