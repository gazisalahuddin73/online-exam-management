@extends('app')

@section('role')
    Admin
@stop

@section('header')
    Teachers
@stop

@section('content') 
    <div class="card">
        <div class="row">
            <div class="col-md-10"></div>
            <div class="col-md-1" style="top: 20px"><a href="{{route('teachers.create')}}"><button class="btn btn-info btn-block">  Add  </button></a></div>       
        </div>
        <div class="card-body" style='margin-top:7px'>
            @if(\Session::has('msg_success'))
            <div class="row">
                <div class="col-lg-10">
                    <div  class="alert">
                        <strong>Success! </strong>{{ \Session::get('msg_success') }}
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>    
            @endif
            <div class="table-responsive">
                <table class="table table-hover mx-auto table-sm">
                    <thead class="header-dark">
                        <tr>
                            <th>Name</th>
                            <th>email</th>
                            <th>Phone</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($teachers as $teacher)
                        <tr>
                            <td>{{$teacher->name}}</td>
                            <td>{{$teacher->email}}</td>
                            <td>{{$teacher->phone}}</td>
                            <td>
                                @if($teacher->is_active == 1)
                                    <button type="button" value="{{$teacher->id}}" class="activate  btn btn-danger">Deactivate</button>
                                @else
                                    <button type="button" value="{{$teacher->id}}" class="activate  btn btn-info">Activate</button>
                                @endif
                            </td>
                            <td> 
                                <i class="nav-icon fas fa-cogs gear"></i>
                                <div style='display:none' class="btn-group" role="group" aria-label="">
                                    <a href="{{route('teachers.edit',[$teacher->id])}}"><button type="button" class="btn btn btn-secondary btn-info">Edit</button></a>
                                    <span style="margin-left: 10px"></span>
                                    <form class='delete' method="post" action="{{route('teachers.destroy', [$teacher->id])}}" enctype="multipart/form-data">
                                        @csrf @method('DELETE')
                                        <button type="submit" class="btn btn btn-secondary btn-danger">Delete</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('.activate').click(function(e){
                    e.preventDefault();
                    if ($(this).text().trim()==='Deactivate'){
                        $(this).text('Activate')
                        $(this).removeClass('activate btn btn-danger')
                        $(this).addClass('activate btn btn-info')
                    }else if ($(this).text().trim()==='Activate'){
                        $(this).text('Deactivate')
                        $(this).removeClass('activate btn btn-info')
                        $(this).addClass('activate btn btn-danger')
                    }

                    $.ajax({
                        type:'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:'{{url('')}}'+'/teacher/activation/'+$(this).val(),
                        data:'_token = <?php echo csrf_token() ?>',
                        success:function() {

                        }
                    })
                }
            );
        });
</script>

<script>
    $(".delete").on("submit", function(){
        return confirm("Are you sure?");
    });
</script>

<script>
    $(".gear").on("click", function(){
        $(this).hide()
        $(this).next().removeAttr("style");
    });
</script>
@stop
