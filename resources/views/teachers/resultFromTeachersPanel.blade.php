@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="card-body">
          <table class="table">
              <thead>
               <tr>
                   <td>Exam</td>
                   <td>Student</td>
                   <td>Right Answers</td>
                   <td>Wrong Answers</td>
                   <td>Answer Sheet</td>
               </tr>
              </thead>
              <tbody>
               @foreach($results as $result)
               <tr>
                    <td>{{$result->exam->name}}</td>
                    <td>{{$result->student->name}}</td>
                    <td>@php echo count(unserialize($result->right)); @endphp</td>
                    <td>@php echo count(unserialize($result->wrong)); @endphp</td>
                    <td><a href="{{route('result.sheet', [$result->id ,$result->exam->id, $result->student->id])}}"><button>View</button></a></td>
               </tr>
               @endforeach
              </tbody>
          </table>
        </div>
    </div>
@stop
