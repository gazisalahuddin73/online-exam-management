@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
          <div class="row">
              <div class="col-md-12">
                  <table class="table">
                      <thead>
                       <tr>
                           <td>Title</td>
                           <td>Exams</td>
                       </tr>
                      </thead>
                      <tbody>
                      @foreach($exam_list as $item)
                      <tr>
                           <td>{{$item['title']}}</td>
                           <td>
                               @foreach($item['exam_ids'] as $id)
                                   <ul>
                                       <li>
                                           <a href="{{route('exam.attend', [$id, \Auth::user()->id])}}">{{\App\Exam::find($id)->name}}</a>
                                       </li>
                                   </ul>
                                   @endforeach
                           </td>
                       </tr>
                      @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
        </div>
    </div>
@stop
