@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="container-fluid">
                <!-- Content Row -->
                <div class="row">

                    <!-- Content Column -->
                    <div class="col-lg-12 mb-4">

                        <!-- Project Card Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Result</h6>
                            </div>
                            <div>
                                @foreach($questions as $question )
                                    @php
                                        $current_question = \App\Question::with('summernote')->find($question);
                                        $current_options = unserialize( $current_question->options);
                                        $correct_option = unserialize($current_question->correct);
                                        $letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'];
                                    @endphp
                                    @if(!in_array($question, $no_response))
                                        @if(in_array($question, $right))
                                            @php
                                                $response = 'right';
                                                unset($answered_option);
                                            @endphp
                                        @endif

                                        @if(in_array($question, $wrong))
                                            @php
                                                foreach ($original_responses as $original_response){
                                                    if ($original_response[0] == $question){
                                                        $answered_option = $original_response[1];
                                                        break;
                                                    }
                                                }

                                                $response = 'Wrong';
                                            @endphp
                                        @endif
                                    @else
                                        @php
                                            $response = 'No response';
                                            unset($answered_option)
                                        @endphp
                                    @endif

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <td>Question</td>
                                            <td>Options</td>
                                            <td>Status</td>
                                            <td>Correct Option</td>
                                            <td>Answered Option</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <h2>{!! $current_question->summernote->instructions!!}</h2>
                                            </td>
                                            <td>
                                                @php $i=0; @endphp
                                                @foreach($current_options as $option)
                                                    @php $i++; @endphp
                                                    <ul>
                                                        <li> {{$letters[$i-1]}}: {{$option}}</li>
                                                    </ul>
                                                @endforeach
                                            </td>
                                            <td>{{$response}}</td>
                                            <td><h6>Option: {{$letters[$correct_option[0]]}}</h6></td>
                                            <td>
                                                @if(isset($answered_option))
                                                    <h6>Option: {{$answered_option}}</h6>
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
