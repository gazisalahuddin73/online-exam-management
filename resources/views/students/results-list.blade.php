@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Content Row -->
                <div class="row">

                    <!-- Content Column -->
                    <div class="col-lg-6 mb-4">

                        <!-- Project Card Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Exams</h6>
                            </div>
                            <div>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td>Exam Name</td>
                                        <td>Result</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($results as $result)
                                        <tr>
                                            <td>
                                                {{$result->exam->name}}
                                            </td>
                                            <td>
                                                <a href="{{route('student.result.sheet', [$result->id ,$result->exam->id, $student->id])}}"><button class="btn btn-primary">View</button></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->
        </div>
    </div>
    @stop
