@extends('app')

@section('header')
<h4 style="font-family:verdana">Exam wise Student List</h4>
@endsection

@section('content')
<div class="card">
        <div class="card-body">    
            <div class="table-responsive"> 
                <table class="table table-borderless">
                    <thead>
                     <tr>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td><a href="{{url('/student/add/to/exams')}}"><button class="btn btn-primary">Add New</button></a></div></td>
                     </tr>
                    </thead>
                </table>
            </div> 

            <div class="table-responsive"> 
                <table class="table table-hover mx-auto table-sm">
                    <thead class="thead-light">
                     <tr>
                         <th></th>
                         <th></th>
                         <th></th>
                         <th></th>
                         <th></th>
                         <th>Exam</th>
                         <th>Students</th>
                         <th>Action</th>
                     </tr>
                    </thead>
                    <tbody>
                     @foreach($exam_students as $item)
                     <tr>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td>{{$item->exam->name}}</td>
                         <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">View</button>
                         <!-- Modal -->
                         <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                             <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                     <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLabel">Students</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                         @php
                                             $unserialized = unserialize($item->students);
                                             $students = \App\User::whereIn('id', $unserialized)->with('institution')->get();
                                         @endphp
                                         <table class="table">
                                             <thead>
                                              <tr>
                                                  <td>Name</td>
                                                  <td>Institute</td>
                                                  <td>Class</td>
                                                  <td>Roll No.</td>
                                                  <td>Action</td>
                                              </tr>
                                             </thead>
                                             <tbody>
                                             @foreach($students as $student)
                                                 <tr>
                                                   <td>{{$student->name}}</td>
                                                   <td>{{$student->institution->name}}</td>
                                                   <td>{{$student->class}}</td>
                                                   <td>{{$student->roll_no}}</td>
                                                   <td><a href="{{url('student/unset/from/list', [$item->exam_id, $student->id])}}"><button class="btn-primary">Unset</button></a></td>
                                                 </tr>
                                             @endforeach
                                             </tbody>
                                         </table>
                                     </div>
                                     <div class="modal-footer">
                                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         </td>
                         <td>
                             <div class="btn-group" role="group">
                                <a href="{{route('student-exam.edit.form', $item->id)}}"><button class="btn btn-info">Edit</button></a>
                                <a href="{{url('exam-students/delete', $item->id)}}"><button class="btn btn-danger">Delete</button></a>
                             </div>
                         </td>
                     </tr>
                     @endforeach
                    </tbody>
                </table>
            </div>  
        </div> 
    </div>
@stop

