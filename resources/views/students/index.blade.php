@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
        @if (\Session::has('msg_success'))
                    <div  class="alert ">
                        <strong>Success! </strong>{{ \Session::get('msg_success') }}
                    </div>
                    <div class="clearfix"></div>
                @endif
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><a href="{{route('student.create')}}"><button class="btn btn-primary btn-block">Add New</button></a></th>
                    </tr>
                </thead>
            </table>
        </div>
            
            
                  <div class="table-responsive">
                    <table class="table table-hover mx-auto table-sm">
                        <thead class="thead-light">
                        <tr>
                            <th>Name</th>
                            <th>Institution</th>
                            <th>Class</th>
                            <th>Roll No.</th>
                            <th>Approval</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($students->count() != 0)
                            @foreach($students as $student)
                                <tr>
                                    <td>{{$student->name}}</td>
                                    <td>{{$student->institution->name}}</td>
                                    <td>{{$student->class}}</td>
                                    <td>{{$student->roll_no}}</td>
                                    <td>
                                        @if($student->is_active == 1)
                                            <button type="button" value="{{$student->id}}" class="approve  btn btn-danger">Disapprove</button>
                                              @else
                                            <button type="button" value="{{$student->id}}" class="approve  btn btn-info">Approve</button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td><h1>Please add some students first</h1></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                  </div>  
                
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.approve').click(function(e){
                    e.preventDefault();
                if ($(this).text()==="Disapprove"){
                    $(this).text("Approve");
                    $(this).removeClass('btn btn-danger');
                    $(this).addClass('btn btn-info');
                } else if($(this).text()==="Approve") {
                    $(this).text("Disapprove");
                    $(this).removeClass('btn btn-info');
                    $(this).addClass('btn btn-danger');
                }

                $.ajax({
                        type:'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:'{{url('')}}'+'/student/approval/'+$(this).val(),
                        data:'_token = <?php echo csrf_token() ?>',
                        success:function(data) {
                            if(data.msg=='Disapprove'){
                                
                            }else if(data.msg=='Approve'){
                               
                            }
                        }
                    })
                }
            )
        });
    </script>
@stop
