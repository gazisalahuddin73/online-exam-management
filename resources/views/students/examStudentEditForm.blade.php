@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <h4 class="col-md-2">Exam</h4>
                        <select name="exam" id="exam">
                           <option selected value="{{$examStudent->exam->id}}">{{$examStudent->exam->name}}</option>
                        </select>
                    </div>
                    <br>
                    <div class="row">
                        <h4 class="col-md-2">Institution</h4>
                        <select name="institution" id="institution">
                            <option disabled selected>Select</option>
                            @foreach($institutions as $institution)
                                <option value={{$institution->id}}>{{$institution->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <br>
                    <div class="row">
                        <h4 class="col-md-2">Class</h4>
                        <select name="class" class="class col-md-6">
                            @for($i=0; $i<=10; $i++)
                                <option value={{$i}}>{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <br>
                    <div class="row">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <td>Name</td>
                                <td>Roll No.</td>
                                <td>Select</td>
                            </tr>
                            </thead>
                            <tbody id="list">

                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div class="row">
                        <h4>List of the selected students</h4>
                        @php
                            $unserialized = unserialize($examStudent->students);
                            $students = \App\User::whereIn('id', $unserialized)->with('institution')->get();
                        @endphp
                        <table class="table">
                            <thead>
                            <tr>
                                <td>Name</td>
                                <td>Institute</td>
                                <td>Class</td>
                                <td>Roll No.</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody id="students">
                            @foreach($students as $student)
                                <tr>
                                    <td>{{$student->name}}</td>
                                    <td>{{$student->institution->name}}</td>
                                    <td>{{$student->class}}</td>
                                    <td>{{$student->roll_no}}</td>
                                    <td><a href="{{url('student/unset/from/list', [$examStudent->exam_id, $student->id])}}"><button class="btn-primary">Unset</button></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.class').click(function(e){
                    e.preventDefault();
                    let institution = $('#institution').val();
                    $.ajax({
                        type:'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:'{{url('')}}'+'/student/list/from/institution/'+`${institution}`+'?class='+$(this).val(),
                        data:{exam_id: $('#exam').val()},
                        success:function(data) {
                            $('#list').html(data.msg);
                        }
                    })
                }
            );
            $('#list').click(function(e){
                e.preventDefault();
                let exam_id = $('#exam').val();
                let checkedInput = $('input[type=checkbox]:checked');
                if(checkedInput){
                    var student_id= checkedInput.next('input[type=hidden]').val();
                }

                $.ajax({
                    type:'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'{{url('')}}'+'/student/attach/to/exam/'+`${student_id}`+'?exam_id='+`${exam_id}`,
                    data:'_token = <?php echo csrf_token() ?>',
                    success:function(data) {
                        $('#students').append(data.msg);
                        checkedInput.parent().parent().remove();
                    }
                })
            }
            );
            $('#students').click(
                $(this).remove()
            )
        });
    </script>
@stop
