@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <h4 class="col-md-2">Select Exam</h4>
                        <select name="exam" id="exam">
                            <option disabled selected>Select</option>
                            @foreach($exams as $exam )
                                <option value="{{$exam->id}}">{{$exam->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <br>
                    <div class="row">
                        <h4 class="col-md-2">Institution</h4>
                        <select name="institution" id="institution">
                            <option disabled selected>Select</option>
                            @foreach($institutions as $institution)
                                <option value={{$institution->id}}>{{$institution->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <br>
                    <div class="row">
                        <h4 class="col-md-2">Class</h4>
                        <select name="class" class="class col-md-6">
                            <option disabled selected>Class</option>
                            @for($i=1; $i<=10; $i++)
                                <option value={{$i}}>{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <br>
                    <div class="row">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <td>Name</td>
                                <td>Roll No.</td>
                                <td>Select</td>
                            </tr>
                            </thead>
                            <tbody id="list">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.class').click(function(e){
                    e.preventDefault();
                    let institution = $('#institution').val();
                    $.ajax({
                        type:'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:'{{url('')}}'+'/student/list/from/institution/'+`${institution}`+'?class='+$(this).val(),
                        data:{exam_id: $('#exam').val()},
                        success:function(data) {
                            $('#list').html(data.msg);
                        }
                    })
                }
            );
            $('#list').click(function(e){
                e.preventDefault();
                let exam_id = $('#exam').val();
                let checkedInput = $('input[type=checkbox]:checked');
                if(checkedInput){
                    var student_id= checkedInput.next('input[type=hidden]').val();
                }

                $.ajax({
                    type:'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'{{url('')}}'+'/student/attach/to/exam/'+`${student_id}`+'?exam_id='+`${exam_id}`,
                    data:'_token = <?php echo csrf_token() ?>',
                    success:function(data) {
                        checkedInput.parent().parent().remove();
                    }
                })
            })
        });
    </script>
@stop
