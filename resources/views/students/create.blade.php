@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{route('student.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <h4 class="col-md-2" style="font-family:verdana">Name</h4>
                            <input required class="col-md-6" name="name" class="form-control">
                        </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-2" style="font-family:verdana">Institution</h4>
                            <select name="institution_id">
                                <option disabled selected>Select</option>
                            @foreach($institutions as $institution)
                              <option value={{$institution->id}}>{{$institution->name}}</option>
                            @endforeach
                            </select>
                        </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-2" style="font-family:verdana">Class</h4>
                            <select name="class" class="col-md-6">
                                @for($i=1; $i<=10; $i++)
                                    <option value={{$i}}>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-2" style="font-family:verdana">Roll No.</h4>
                            <input required class="col-md-6" name="roll_no" class="form-control">
                        </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-2" style="font-family:verdana">Phone No.</h4>
                            <input required class="col-md-6" name="phone" class="form-control">
                        </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-2" style="font-family:verdana">E-mail address</h4>
                            <input class="col-md-6" name="email" class="form-control">
                        </div>
                        <br>
                        <div class="row">
                                <h4 class="col-md-2" style="font-family:verdana">Password</h4>
                                <input id="password" type="password" class=" @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>
                        <br>
                        <div class="row">
                            <h5 class="col-md-2" style="font-family:verdana">Confirm Password</h5>
                            <div class="">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Save</button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ul>
                    @foreach ($errors->all() as $message) {
                      <li>{{$message}}</li>
                    }
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop
