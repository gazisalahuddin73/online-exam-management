@extends('app')

@section('css')
<link href="{{asset('css/mcq-paper.css')}}" media="screen" rel="stylesheet" type="text/css">
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <div id="clockdiv" class='row'>
             <div class="col-md-3">
                    <span class="days"></span>
                    <div class="smalltext">Days</div>
              </div>  
              <div class="col-md-3">
                    <span class="hours"></span>
                    <div class="smalltext">Hours</div>
              </div>
              <div class="col-md-3">
                    <span class="minutes"></span>
                    <div class="smalltext">Minutes</div>
              </div>
              <div class="col-md-3">
                    <span class="seconds"></span>
                    <div class="smalltext">Seconds</div>
              </div>
        </div>
        <div class="row">
         <div class="col-md-12">   
            <div class="quiz-container">
                <div id="quiz"></div>
            </div>
            <button id="previous">Previous Question</button>
            <button id="next">Next Question</button>
            <button class="submit" id="submit">Submit Quiz</button>
            <div id="results"></div>
         </div>   
        </div>
    </div>
</div>        
@stop

@section('js')
      <script>
        (function(){
            // Functions
            function buildQuiz(){
                // variable to store the HTML output
                const output = [];
                // for each question...
                myQuestions.forEach(
                    (currentQuestion, questionNumber) => {
                        // variable to store the list of possible answers
                        const answers = [];
                        // and for each available answer...
                        for(letter in currentQuestion.answers){
                            // ...add an HTML radio button
                            answers.push(
                                `<label>
                                    <input class="radio" type="radio" name="question${questionNumber}" value="${letter}">
                                    ${letter} :
                                    ${currentQuestion.answers[letter]}
                                </label>`
                            );
                        }

                        // add this question and its answers to the output
                        output.push(
                            `<div class="slide">
                                <div class="question"> <p style="text-align:left; padding: 5px; font-weight: 150;"> ${currentQuestion.question}</p> </div>
                                <div id=${currentQuestion.question_id.id} class="question_id"></div>
                                <div class="answers"> ${answers.join("")} </div>
                             </div>`
                        );
                    }
                );
                // finally combine our output list into one string of HTML and put it on the page
                quizContainer.innerHTML = output.join('');
            }

            function showSlide(n) {
                slides[currentSlide].classList.remove('active-slide');
                slides[n].classList.add('active-slide');
                currentSlide = n;
                if(currentSlide === 0){
                    previousButton.style.display = 'none';
                }
                else{
                    previousButton.style.display = 'inline-block';
                }
                if(currentSlide === slides.length-1){
                    nextButton.style.display = 'none';

                }
                else{
                    nextButton.style.display = 'inline-block';

                }
            }

            function showNextSlide() {
                showSlide(currentSlide + 1);
            }

            function showPreviousSlide() {
                showSlide(currentSlide - 1);
            }

            // Variables
            const quizContainer = document.getElementById('quiz');

            const myQuestions = [
                {!! $questions !!}
            ];

            // Kick things off
            buildQuiz();

            // Pagination
            const previousButton = document.getElementById("previous");
            const nextButton = document.getElementById("next");
            const slides = document.querySelectorAll(".slide");
            let currentSlide = 0;

            // Show the first slide
            showSlide(currentSlide);

            // Event listeners
            previousButton.addEventListener("click", showPreviousSlide);
            nextButton.addEventListener("click", showNextSlide);
        })();

    </script>

    <script type="text/javascript">

        $(document).ready(function(){
            $('.submit').click(function(e){
                    e.preventDefault();
                    var divs = $('.question_id'), i;
                    let questionIDs = [];
                    let checkedOptions = [];

                    for (i = 0; i < divs.length; ++i) {
                        questionIDs.push($(divs[i]).attr('id'));
                    }
                    $(".radio:checked").each(function() {
                        let question_id = $(this).parent().parent().parent().children("div.question").next().attr('id');
                        checkedOptions.push([question_id, $(this).val()]);
                    });

                    $.ajax({
                        type:'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:'{{url('')}}/result/create/{{ $exam->id }}/{{$student->id}}',
                        data:{questionIDs: questionIDs, checkedOptions:checkedOptions},
                        success: function(res){
                            window.location=res.url;
                        }
                    })
                }
            );
        });
    </script>

    <script src=" {{asset('js/moment.js')}}"></script>
    <script>
        const format1 = "YYYY-MM-DD HH:mm:ss";

        if ((moment('{{$exam_end}}').format(format1)) < moment().format(format1)){
            document.getElementById('quiz').style.display="none";
            document.getElementById('next').style.display="none";
            document.getElementById('previous').style.display="none";
            document.getElementById('submit').style.display="none";
            document.getElementById('clockdiv').style.display="none";
        }

        function getTimeRemaining(endtime) {
            const total = Date.parse(endtime) - Date.parse(new Date());
            const seconds = Math.floor((total / 1000) % 60);
            const minutes = Math.floor((total / 1000 / 60) % 60);
            const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
            const days = Math.floor(total / (1000 * 60 * 60 * 24));

            return {
                total,
                days,
                hours,
                minutes,
                seconds
            };
        }

        function initializeClock(id, endtime) {
            const clock = document.getElementById(id);
            const daysSpan = clock.querySelector('.days');
            const hoursSpan = clock.querySelector('.hours');
            const minutesSpan = clock.querySelector('.minutes');
            const secondsSpan = clock.querySelector('.seconds');

            function updateClock() {
                const t = getTimeRemaining(endtime);

                daysSpan.innerHTML = t.days;
                hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

                if (t.days>=0){
                    if (t.total <= 0) {
                        clearInterval(timeinterval);
                        document.getElementById('quiz').style.display="inline";
                        document.getElementById('next').style.display="inline";
                        document.getElementById('previous').style.display="inline";
                        document.getElementById('submit').style.display="inline";
                        document.getElementById('clockdiv').style.display="none";
                    }else if (t.total > 0){
                        document.getElementById('quiz').style.display="none";
                        document.getElementById('next').style.display="none";
                        document.getElementById('previous').style.display="none";
                        document.getElementById('submit').style.display="none";
                    }
                }else {
                    document.getElementById('clockdiv').style.display="none";
                }
            }

            updateClock();
            const timeinterval = setInterval(updateClock, 1000);
        }

        const deadline = moment('{{$exam_start}}').format(format1);console.log(deadline)
        initializeClock('clockdiv', deadline);
    </script>
@stop
