@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="container-fluid">
                <!-- Content Row -->
                <div class="row">

                    <!-- Content Column -->
                    <div class="col-lg-6 mb-4">

                        <!-- Project Card Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Exams</h6>
                            </div>
                            <div>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td>Exam Name</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($exam_students as $exam_student)
                                        <tr>
                                            <td>
                                                @if(in_array($student->id, unserialize($exam_student->students)))
                                                    @if(!App\Result::where(['exam_id'=> $exam_student->exam_id, 'student_id'=>$student->id ])->exists())
                                                        @if(\Carbon\Carbon::parse(App\Exam::find($exam_student->exam_id)->date_time)->gt(\Carbon\Carbon::now()))
                                                           @php
                                                               $exam_used_in_competition = '';
                                                               $competitions = App\Competition::all();
                                                           @endphp
                                                           @foreach($competitions as $competition)
                                                                @if (in_array($exam_student->exam_id, unserialize($competition->exam_ids)))
                                                                    @php $exam_used_in_competition = true; @endphp
                                                                @endif
                                                           @endforeach
                                                           @if($exam_used_in_competition!=true)
                                                            <a href="{{route('exam.attend', [$exam_student->exam->id, $student->id])}}">{{$exam_student->exam->name}}</a>
                                                           @endif
                                                        @endif
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop

@section('js')

@stop
