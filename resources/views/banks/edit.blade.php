@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <h1 id="top">{{$bank->name}}</h1>
                        <div class="row">
                            <h4 class="col-md-2">Question Set</h4>
                                <form action="{{url('bank/rename', [$bank->id])}}" method="post">
                                    <input required class="col-md-6" name="name" class="form-control" id="name" value="{{$bank->name}}">
                                    @csrf
                                    <button type="submit" class="btn btn-info">Rename</button>
                                </form>
                            </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-2">Subject:</h4>
                            <select class="col-md-6" title="subject" name="subject_id" id="select">
                                <option selected disabled>Select</option>
                                @foreach($subjects as  $subject)
                                    <option value="{{$subject->id}}"> {{$subject->name}} </option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        <div class="card">
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td>Question</td>
                                        
                                        <td>Marks</td>
                                        <td>Negative Marks</td>
                                        <td>Subject</td>
                                        
                                        <td>Select</td>
                                    </tr>
                                    </thead>
                                    <tbody id="question_selector">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <h5>Selected Questions List</h5>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td>Question</td>
                                        
                                        <td>Marks</td>
                                        <td>Negative Marks</td>
                                        <td>Subject</td>
                                        
                                        <td>Actions</td>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    @if(isset($questions))
                                        @foreach($questions as $question)
                                            <tr id="_{{$question->id}}">
                                                <td>{!!$question->summernote->instructions!!}</td>

                                                <td>{{$question->mark}}</td>
                                                <td>{{$question->negative}}</td>
                                                <td>{{$question->subject->name}}</td>
                                                
                                                <td>
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <button value="{{$question->id}}" class="unset btn btn-danger">Unset</button>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td><h1>Please add some questions first</h1></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#select').change(function(){
                $.ajax({
                    type:'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'{{url('')}}/banks/question/select/while/edit/{{$bank->id}}/'+$(this).val(),
                    data:{},
                    success:function(data) {
                        $("#question_selector").html(data.msg);
                    }
                })
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#question_selector').change(function(){
                $.ajax({
                    type:'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'{{url('')}}/banks/question/set/while/edit/{{$bank->id}}/'+$('input[name="question_id"]:checked').val(),
                    data:{name: $('#name').val()},
                    success:function(data) {
                        $('#top').html(data.name);
                        $('#select').val('select').prop('selected', true);
                        $("#question_selector").html(' ');
                        $("#tbody").html(data.list);
                    }
                })
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            //$('.unset').click(function(){
            $(document).on("click", ".unset", function(){
                    $.ajax({
                        type:'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:'{{url('')}}/banks/question/unset/'+$(this).val(),
                        data:{bank_id: "{{$bank->id}}" },
                        success:function(data) {
                            $('#select').val('select').prop('selected', true);
                            $("#question_selector").html(data.msg);
                            $("#tbody").html(data.message);
                        }
                    })
                }
            );
        });
    </script>
@stop
