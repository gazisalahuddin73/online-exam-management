@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                        <div class="row">
                            <h4 class="col-md-2">Question Set</h4>
                            <input required class="col-md-6" name="name" class="form-control" id="name">
                            <h6 id="msg"></h6>
                        </div>
                        <br>
                        <div class="row">
                            <h4 class="col-md-2">Subject:</h4>
                            <select class="col-md-6" title="subject" name="subject_id" id="select">
                                <option disabled selected>Select</option>
                                @foreach($subjects as  $subject)
                                    <option value="{{$subject->id}}"> {{$subject->name}} </option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        <div class="card">
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td>Question</td>
                                        
                                        <td>Marks</td>
                                        <td>Negative Marks</td>
                                        <td>Subject</td>
                                        
                                        <td>Select</td>
                                    </tr>
                                    </thead>
                                    <tbody id="question_selector">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <h5>Selected Questions List</h5>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td>Question</td>
                                        
                                        <td>Marks</td>
                                        <td>Negative Marks</td>
                                        <td>Subject</td>
                                        
                                        <td>Actions</td>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    @if(isset($questions))
                                        @foreach($questions as $question)
                                            <tr id="_{{$question->id}}">
                                                <td>{!!$question->summernote->instructions!!}</td>

                                                <td>{{$question->mark}}</td>
                                                <td>{{$question->negative}}</td>
                                                <td>{{$question->subject->name}}</td>
                                                
                                                <td>
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <button value="{{$question->id}}" class="unset btn btn-danger">Unset</button>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td><h6>Please add some questions first</h6></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#select').change(function(){
                $.ajax({
                    type:'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'{{url('')}}/banks/question/select/while/create/'+$(this).val(),
                    data:{name: $('#name').val()},
                    success:function(data) {
                        $("#question_selector").html(data.msg);
                    }
                })
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#question_selector').change(function(){
                $.ajax({
                    type:'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'{{url('')}}/banks/question/set/while/create/'+$('input[name="question_id"]:checked').val(),
                    data:{name: $('#name').val()},
                    success:function(data) {
                        $('#select').val('select').prop('selected', true);
                        $("#question_selector").html(' ');
                        $('#msg').html(data.message);
                        $("#tbody").html(data.list);
                    }
                })
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            //$('.unset').click(function(){
            $(document).on("click", ".unset", function(){
                    $.ajax({
                        type:'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:'{{url('')}}/banks/question/unsetWhileCreate/'+$(this).val(),
                        data:{name: $('#name').val()},
                        success:function(data) {
                            $('#select').val('select').prop('selected', true);
                            $("#question_selector").html(data.msg);
                            $("#tbody").html(data.message);
                        }
                    })
                }
            );
        });
    </script>
@stop
