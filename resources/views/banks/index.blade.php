@extends('app')

@section('header')
 Question Banks
@stop

@section('content')
    <div class="card">
        <div class="card-body">
        @if (\Session::has('msg_success'))
        <div class="row">
            <div class="col-lg-10">
                <div class="alert ">
                    <strong>Success! </strong>{{ \Session::get('msg_success') }}
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        @endif
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr> 
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><a href="{{route('banks.create')}}"><button class="btn btn-primary btn-block">Add New</button></a></th>
                    </tr>
                </thead>
            </table>
        </div>
       
                 <div class="table-responsive">
                    <table class="table table-hover mx-auto table-sm">
                        <thead class="thead-light">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Title</th>
                            <th>Number of questions</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($banks->count() != 0)
                            @foreach($banks as $bank)
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>{{$bank->name}}</td>
                                    <td>
                                        @php
                                            $unserialized = unserialize($bank->question_ids);
                                            $questions = count($unserialized);
                                        @endphp
                                        {{$questions}}
                                    </td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                <form action="{{route('banks.destroy', $bank->id)}}" method="post">
                                                    @csrf @method('delete')
                                                    <button class="btn btn-danger">Delete</button>
                                                </form>

                                                <form action="{{route('banks.edit', $bank->id)}}" method="get">
                                                    @csrf
                                                    <button class="btn btn-info">Edit</button>
                                                </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td><h6>Please add some questions first</h6></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                 </div>   
                
    </div>
</div>
@stop
