<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Faker\Generator as Faker;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        factory(\App\Question::class, 50)->create()->each(function ($question) {
            $question->body = rand(1, 9);
            $question->subject_id = rand(1, 2);
            $question->mark = 1;
            $question->options = serialize([Str::random(10), Str::random(10), Str::random(10), Str::random(10)]);
            $question->correct = serialize([1]);
            $question->order = 1;
            $question->save();
        });
        factory(\App\Summernote::class, 10)->create()->each(function ($summernote) {
            $summernote->instructions = '<p>'.Str::random(10).'</p>';
            $summernote->save();
        });

       
        //Admin role start
        $admin = Role::create(['name' => 'Admin', 'guard_name'=> 'web']);
        //Teacher role related
        $teacher = Role::create(['name' => 'Teacher', 'guard_name'=>'web']);
        $permission = Permission::create(['name' => 'manage accessibility', 'guard_name'=>'web']);
        $admin->givePermissionTo($permission);
        $permission->assignRole($admin);

        $permission = Permission::create(['name' => 'add teacher', 'guard_name'=>'web']);
        $admin->givePermissionTo($permission);
        $permission->assignRole($admin);

        $permission = Permission::create(['name' => 'edit exam', 'guard_name'=>'web']);
        $admin->givePermissionTo($permission);
        $teacher->givePermissionTo($permission);
        $permission->assignRole($admin);

        $permission = Permission::create(['name' => 'create exam', 'guard_name'=>'web']);
        $admin->givePermissionTo($permission);
        $teacher->givePermissionTo($permission);
        $permission->assignRole($admin);

        $permission = Permission::create(['name' => 'delete exam', 'guard_name'=>'web']);
        $admin->givePermissionTo($permission);
        $teacher->givePermissionTo($permission);
        $permission->assignRole($admin);

        $permission = Permission::create(['name' => 'activate exam', 'guard_name'=>'web']);
        $admin->givePermissionTo($permission);
        $teacher->givePermissionTo($permission);
        $permission->assignRole($admin);

        $permission = Permission::create(['name' => 'create question', 'guard_name'=>'web']);
        $admin->givePermissionTo($permission);
        $teacher->givePermissionTo($permission);
        $permission->assignRole($admin);

        $permission = Permission::create(['name' => 'edit question', 'guard_name'=>'web']);
        $admin->givePermissionTo($permission);
        $teacher->givePermissionTo($permission);
        $permission->assignRole($admin);

        $permission = Permission::create(['name' => 'delete question', 'guard_name'=>'web']);
        $admin->givePermissionTo($permission);
        $teacher->givePermissionTo($permission);
        $permission->assignRole($admin);


        //Student role related
        $student = Role::create(['name' => 'Student', 'guard_name'=> 'web']);
        $permission_for_student = Permission::create(['name' => 'do exam', 'guard_name'=> 'web']);
        $student->givePermissionTo($permission_for_student);
        $permission_for_student->assignRole($student);

        DB::table('model_has_roles')->insert([
            'role_id' => 1,
            'model_type' => 'App\User',
            'model_id' => 1
        ]);

        \App\User::create(['name'=> 'Mr. Admin', 'email'=>'admin@admin.com', 'phone'=>'01719269063','password'=> \Hash::make(123456789), 'is_admin'=>1]);
    }
}
