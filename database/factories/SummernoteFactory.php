<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Summernote;
use Faker\Generator as Faker;

$factory->define(Summernote::class, function (Faker $faker) {
    return [
        'instructions' => '<p>'.Str::random(10).'</p>>'
    ];
});
