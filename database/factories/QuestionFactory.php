<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
    'body' => rand(1, 9),
    'subject_id' => rand(1, 9),
    'mark' => 1,
    'options' => serialize([Str::random(10), Str::random(10), Str::random(10), Str::random(10)]),
    'correct' =>  serialize([1]),
    'order' => 1
    ];
});
