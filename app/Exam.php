<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $dates = ['date_time'];
    protected $guarded = [];

    public function group(){
        return $this->belongsTo('App\Group');
    }

    public function questions(){
        return $this->belongsToMany('App\Question')->using('App\ExamQuestion');
    }

    public function summernote(){
        return $this->belongsTo('App\Summernote');
    }
}
