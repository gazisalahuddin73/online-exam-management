<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = [];

    public function exam(){
        return $this->belongsTo('App\Exam');
    }

    public function subject(){
        return $this->belongsTo('App\Subject');
    }


    public function summernote(){
        return $this->belongsTo('App\Summernote', 'body', 'id');
    }
}
