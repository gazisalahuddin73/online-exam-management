<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamStudent extends Model
{
    protected $table = 'exam_students';
    protected $guarded = [];

    public function exam(){
        return $this->belongsTo('App\Exam');
    }
}
