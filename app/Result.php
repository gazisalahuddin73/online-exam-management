<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $guarded = [];

    public function student(){
        return $this->belongsTo('App\User');
    }

    public function exam(){
        return $this->belongsTo('App\Exam');
    }
}
