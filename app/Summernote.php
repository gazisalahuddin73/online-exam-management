<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Summernote extends Model
{

    function escape_like($string)
    {
        $search = ['<p>'];
        $replace   = [' '];
        return str_replace($search, $replace, $string);
    }
}
