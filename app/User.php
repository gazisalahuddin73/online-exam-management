<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasRoles;
    protected $guard = 'web';

    protected $fillable = [
        'name', 'email', 'password', 'phone', 'institution_id', 'class', 'roll_no'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->hasOne('App\Role');
    }

    
    public function isAdmin()
    {
       if ($this->role()->name == 'Admin')
            {
                return true;
            }
        return false;
    }

    public function institution(){
        return $this->belongsTo('App\Institution');
    }
}
