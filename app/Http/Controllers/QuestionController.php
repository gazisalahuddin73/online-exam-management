<?php

namespace App\Http\Controllers;

use App\Exam;
use App\ExamQuestion;
use App\Question;
use App\Group;
use App\Subject;
use App\Summernote;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index(){
        $questions = Question::orderBy('order')->paginate(10);
        return view('questions.index', compact('questions'));
    }

    public function create(){
        $subjects = Subject::all();
        $groups = Group::all();
        return view('questions.create', compact('groups', 'subjects'));
    }

    public function save(Request $request){
        
        $request->validate([
            'body' => 'required',
            'mark' => 'required|integer',
            'negative' => 'required|max:2',
            'correct' => 'required|string',
            'order' => 'integer|max:2',
            'subject_id'=>'required',
            'option_.*'=>'required'  
        ]);

        $computed_array = [];
        foreach ($request->request as $key=>$val){
            $computed_array[$key] = $val;
        }
        
        $sliced = array_slice($computed_array, 7);
        
        $options = [];
        
        /*
         * Now this loop does three things at once,
         * it filters through and makes array with
         * question options and correct answers
         */
        foreach($sliced as $key=>$value){
            if ("correct" == substr($key,0,7)){
                $correct_ans = $value;
            }elseif ("option_" == substr($key,0,7)){
                $options[$key] = $value;
            }
        }
        $dom = new \DomDocument();
        $dom->loadHTML('<?xml encoding="UTF-8">'. $request->body);
        $detail = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $detail .= $dom->saveHTML( $dom->documentElement );
        $summernote = new Summernote;
        $summernote->instructions = $detail;
        $summernote->save();

        Question::create([
            'body' => $summernote->id,
            'subject_id' => $request->subject_id,
            'mark' => $request->mark,
            'negative' => $request->negative,
            'options' => serialize(array_values($options)),
            'correct' => serialize([intval(str_replace('option_', '', $correct_ans))]),
            'order' => $request->order
        ]);
        \Session::flash('msg_success', 'New question has been created!');
        return back();
    }

    public function edit($id){

        $question = Question::find($id);

        $options = unserialize($question->options);

        $correct = unserialize($question->correct);

        $subjects = Subject::all();

        $subject_name = $question->subject->name;
        
        request()->session()->flash('status', 'Task was successful!');
        return view('questions.edit', compact('subject_name', 'question', 'options', 'correct', 'subjects'));
    }

    public function update(Question $question, Request $request){

        $request->validate([
            'body' => 'required',
            'mark' => 'required|integer',
            'negative' => 'required|max:2',
            'correct' => 'required|string',
            'order' => 'integer|max:2', 
            'option_.*' => 'required',
            'subject_id'=>'required' 
        ]);

        $computed_array = [];
        foreach ($request->request as $key=>$val){
            $computed_array[$key] = $val;
        }

        $sliced = array_slice($computed_array, 7);

        $options = [];
        
        foreach($sliced as $key=>$value){
            if ("correct" == substr($key,0,7)){
                $correct_ans = $value;
            }elseif ("option_" == substr($key,0,7)){
                $options[$key] = $value;
            }
        }

        $dom = new \DomDocument();
        $dom->loadHTML('<?xml encoding="UTF-8">'. $request->body);
        $detail = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $detail .= $dom->saveHTML( $dom->documentElement );

        $summernote =  Summernote::find($question->summernote->id);
        $summernote->instructions = $detail;
        $summernote->save();

        Question::updateOrCreate(
            ['id' => $question->id],
            [
                'body' => $summernote->id,
                'subject_id' => $request->subject_id,
                'mark' => $request->mark,
                'negative' => $request->negative,
                'options' => serialize(array_values($options)),
                'correct' => serialize([intval(str_replace('option_', '', $correct_ans))-1]),
                'order' => $request->order
            ]
        );
        \Session::flash('msg_success', 'The question has been updated!');
        return redirect()->route('questions.index');
    }

    public function destroy(Question $question)
    {
        $question->delete();
        \Session::flash('msg_success', 'the question has been deleted!');
        return back();
    }

    public function unset($id){
        $exam_question = ExamQuestion::where('question_id', $id)->where('exam_id', request()->exam_id)->first();
        $exam_question->delete();

        $exam = Exam::find($exam_question->exam_id);
        $questions_ids =  ExamQuestion::where('exam_id', $exam->id)->get()->toArray();
        $temp = [];
        $msg = '';
        if (count($questions_ids) != 0){
            foreach ($questions_ids as $key){
                $temp[]=$key['question_id'];
            }
            $questions = Question::whereIn('id', $temp)->get();
            foreach ($questions as $question){
                $msg.='<tr>
                            <td>'.$question->summernote->instructions.'</td>
                            <td>'.$question->mark.'</td>
                            <td>'.$question->negative.'</td>
                            <td>'.$question->subject->name.'</td>
                            
                            <td>
                               <div class="row">
                                    <div class="col-sm-3">
                                         <button value="'.$question->id.'" class="unset btn btn-danger">Unset</button>
                                    </div>
                                </div>
                            </td>
                        </tr>';
            }
        }else{
            $msg = 'No question assigned yet!';
        }

        return response()->json(array('message'=> $msg), 200);
    }

    public function select(Subject $subject){

        $question_ids = ExamQuestion::all()->toArray();

        $selected_question_ids = [];
        foreach ($question_ids as $key){
            if ($key['exam_id'] == request()->exam_id){
                $selected_question_ids[] = $key['question_id'];
            }
        }

        $questions = Question::with('subject')->where('subject_id', $subject->id)->WhereNotIn('id', $selected_question_ids)->get();

        $msg = '';
        foreach ($questions as $question){
            $msg.='<tr>
                         <td>'.$question->summernote->instructions.'</td>
                         <td>'.$question->mark.'</td>
                         <td>'.$question->negative.'</td>
                         <td>'.$question->subject->name.'</td>
                         
                         <td><input type="radio" name="question_id" value="'.$question->id.'"></td>
                    </tr>';
        }
        return response()->json(array('message'=> $msg), 200);
    }

    public function set(Question $question){

        ExamQuestion::create(['exam_id'=>request()->exam_id, 'question_id'=>$question->id]);

        $question_ids = ExamQuestion::all()->toArray();

        $selected_question_ids = [];
        foreach ($question_ids as $key){
            if ($key['exam_id'] !== request()->exam_id){
                $selected_question_ids[] = $key['question_id'];
            }
        }

        $questions = Question::with('subject')->where('subject_id', $question->subject_id)->WhereNotIn('id', $selected_question_ids)->get();

        $msg = '';
        foreach ($questions as $question){
            $msg.='<tr>
                         <td>'.$question->summernote->instructions.'</td>
                         <td>'.$question->mark.'</td>
                         <td>'.$question->negative.'</td>
                         <td>'.$question->subject->name.'</td>
                         <td><input type="radio" name="question_id" value="'.$question->id.'"></td>
                    </tr>';
        }


        $questions_ids =  ExamQuestion::where('exam_id', request()->exam_id)->get()->toArray();
        $temp = [];
        $list = '';
        if (count($questions_ids) != 0){
            foreach ($questions_ids as $key){
                $temp[]=$key['question_id'];
            }
            $questions = Question::whereIn('id', $temp)->get();
            foreach ($questions as $question){
                $list.='<tr>
                            <td>'.$question->summernote->instructions.'</td>
                            <td>'.$question->mark.'</td>
                            <td>'.$question->negative.'</td>
                            <td>'.$question->subject->name.'</td>
                            <td>
                               <div class="row">
                                    <div class="col-sm-3">
                                         <button value="'.$question->id.'" class="unset btn btn-danger">Unset</button>
                                    </div>
                                </div>
                            </td>
                        </tr>';
            }
        }else{
            $list = 'No question assigned yet!';
        }
        return response()->json(array('message'=> $msg, 'list'=> $list), 200);
    }
}
