<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class ProfileController extends Controller
{
    public function edit(Request $request){
        if(\Auth::user()->hasRole('Admin')){
            $role_id = 1;
        }elseif(\Auth::user()->hasRole('Teacher')){
            $role_id = 2;
        }elseif(\Auth::user()->hasRole('Student')){
            $role_id = 3; 
        }
        $user = \Auth::user();
        return view('profiles.edit', compact('user', 'role_id'));
    }

    public function update(Request $request, Role $role, $id){
        $request->validate([
            'name' => 'required|max:255',
            'phone' => 'required|regex:/[0-9]{9}/|unique:users,phone,'.\Auth()->user()->id.',id',
            'email' => 'nullable||email|unique:users,email,'.\Auth()->user()->id.',id',
            'password' => 'nullable|confirmed',
        ]);
        
        $user = \Auth::user();
        if($request->filled('password') )
        {
            if($role->id==1){
                $user->updateOrCreate([
                    'id'=>1
                ],[
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'phone'=>$request->phone,
                    'password'=>\Hash::make($request->password)
                ]);
               }else{
                   $user->updateOrCreate([
                       'id'=>$id
                   ],[
                       'name'=>$request->name,
                       'email'=>$request->email,
                       'phone'=>$request->phone,
                       'password'=>\Hash::make($request->password)
                   ]);
               }
        }else{
            if($role->id==1){
                $user->updateOrCreate([
                    'id'=>1
                ],[
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'phone'=>$request->phone
                ]);
               }else{
                   $user = \App\User::find($id);
                   $user->updateOrCreate([
                       'id'=>$id
                   ],[
                       'name'=>$request->name,
                       'email'=>$request->email,
                       'phone'=>$request->phone
                   ]);
               }
        }
        \Session::flash('msg_success', 'profile info has been updated!');
        return redirect()->route('home');
    }
}
