<?php

namespace App\Http\Controllers;

use App\Competition;
use App\Exam;
use App\Question;
use App\User;
use App\Institution;
use App\Result;
use Illuminate\Http\Request;

class CompetitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $competitions = Competition::all();
        return view('competitions.index', compact('competitions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $exams = Exam::all();
        $institutions = Institution::all();
        return view('competitions.create', compact('exams', 'institutions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student_ids = User::whereIn('institution_id', $request->institution_ids)
                 ->whereIn('class', $request->class_selected)
                 ->pluck('id')
                 ->toArray();

        Competition::firstOrCreate(
            ['title'=> $request->title],
            [
             'student_ids'=>serialize($student_ids),
             'exam_ids'=> serialize($request->exam_ids)
            ]
        );
        \Session::flash('msg_success', 'New competition has been created!');
        return response()->json(array('msg'=> 'Done'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Competition  $competition
     * @return \Illuminate\Http\Response
     */
    public function show(Competition $competition)
    {
        $student_ids = unserialize($competition->student_ids);
        $exam_ids = unserialize($competition->exam_ids);
        $final = [];
        foreach ($student_ids as $id){
           $right_answers_hub = [];
           $wrong_answers_hub = [];
           $exists = Result::where('student_id', $id)->whereIn('exam_id', $exam_ids)->exists();
           if ($exists){
               $right_answers = Result::where('student_id', $id)->whereIn('exam_id', $exam_ids)->pluck('right');
               foreach ($right_answers as $item){
                   foreach (unserialize($item) as $value){
                       array_push($right_answers_hub, $value);
                   }
               }
               $wrong_answers = Result::where('student_id', $id)->whereIn('exam_id', $exam_ids)->pluck('wrong');
               foreach ($wrong_answers as $item){
                   foreach (unserialize($item) as $value){
                       array_push($wrong_answers_hub, $value);
                   }
               }
           }

           //now calculate the total marks after deducting the negative value
           $marks_for_right_answers = 0;

           foreach ($right_answers_hub as $idX){
               $marks_for_right_answers += Question::find($idX)->mark;
           }

           $negative_marks_for_wrong_answers = 0;
            foreach ($wrong_answers_hub as $idY){
                $negative_marks_for_wrong_answers += Question::find($idY)->negative;
            }
           $final[]=['student_id'=>$id, 'mark'=> $marks_for_right_answers-$negative_marks_for_wrong_answers];
        }

        return view('competitions.show', compact('final'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Competition  $competition
     * @return \Illuminate\Http\Response
     */
    public function edit(Competition $competition)
    {
        $exams = Exam::all();
        $institutions = Institution::all();
        return view('competitions.edit', compact('competition', 'exams', 'institutions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Competition  $competition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Competition $competition)
    {
        $student_ids = User::whereIn('institution_id', $request->institution_ids)
            ->where('class', $request->class_selected)
            ->pluck('id')
            ->toArray();

        $exams = Exam::whereIn('id', $request->exam_ids)->get();


        $competition->update([
            'title'=> $request->title,
            'student_ids'=>serialize($student_ids),
            'exam_ids'=> serialize($request->exam_ids)
        ]);
        \Session::flash('msg_success', 'The competition has been updated!');
        return response()->json(array('msg'=> 'Done'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Competition  $competition
     * @return \Illuminate\Http\Response
     */
    public function delete(Competition $competition)
    {
        $competition->delete();
        return back();
    }

//    public function studentList(Request $request, Institution $institution){
//        $students = Student::where('institution_id', $institution->id)
//                ->where('class', $request->class)
//                ->get();
//        //this is the main select element
//        $main_select_element = '<select multiple="multiple" id="student_ids" style="position: absolute; left: -9999px;" name="student_ids[]">';
//        foreach ($students as $student){
//            $main_select_element.='<option value="'.$student->name.'">'.$student->name.'</option>';
//        }
//        $main_select_element.='</select>';
//
//        $two_visible_divs = '<div class="ms-container" id="ms-student_ids">
//                              <div class="ms-selectable">
//                                <ul class="ms-list" tabindex="-2">';
//        $count_1 = 1;
//        foreach ($students as $student){
//            $count_1++;
//            $two_visible_divs.='<li class="ms-elem-selectable" id="'.$count_1.'-selectable"><span>'.$student->name.'</span></li>';
//        }
//        $two_visible_divs.='</ul>
//                                </div>
//                                   <div class="ms-selection">
//                                      <ul class="ms-list" tabindex="-2">
//            ';
//        $count_2 = 1;
//        foreach ($students as $student){
//            $count_2++;
//            $two_visible_divs.='<li style="display: none;" class="ms-elem-selection" id="'.$count_2.'-selection"><span>'.$student->name.'</span></li>';
//        }
//        $two_visible_divs.=' </ul>
//                           </div>
//                        </div>';
//
//        return response()->json(['main_select_element'=> $main_select_element, 'two_visible_divs'=>$two_visible_divs], 200);
//    }

public function enrollStudentFrontend(Request $request){
    if(filter_var($request->email, FILTER_VALIDATE_EMAIL)){
        $type = 'email';
       }else{
        $type = 'regex:/(01)[0-9]{9}/';
       }

    $request->validate([
        'email'   => "required|$type",
        'password' => 'required|min:6'
    ]);
    $competition = Competition::where('title', $request->competition)->first();
    $student_ids = unserialize($competition->student_ids);
    
    if(is_numeric($request->get('email'))){
        if (\Auth::guard('web')->attempt(['phone' => $request->get('email'), 'password' => $request->password])) {
            array_push($student_ids, \Auth::user()->id);
            $competition->update([
                'title'=> $request->competition,
                'student_ids'=>serialize($student_ids),
                'exam_ids'=> serialize($competition->exam_ids)
            ]);
            \Session::flash('msg_success', 'You have been enrolled!');
            return redirect()->intended('/home');
        }
    }
    
    if (\Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password])) {
        array_push($student_ids, \Auth::user()->id);
        $competition->update([
            'title'=> $request->competition,
            'student_ids'=>serialize($student_ids),
            'exam_ids'=> serialize($competition->exam_ids)
        ]);
        \Session::flash('msg_success', 'You have been enrolled!');    
        return redirect()->intended('/home');
    }
    return back()->withInput($request->only('email', 'remember'));
}
}
