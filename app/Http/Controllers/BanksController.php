<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Subject;
use App\Question;
use Illuminate\Http\Request;

class BanksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banks = Bank::all();
        return view('banks.index', compact('banks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subjects = Subject::all();
        return view('banks.create', compact('subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function show(Bank $bank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function edit(Bank $bank)
    {
        $subjects = Subject::all();
        $question_ids = unserialize($bank->question_ids);
        $questions = Question::whereIn('id', $question_ids)->get();
        return view('banks.edit', compact('bank', 'subjects', 'questions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bank $bank)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bank $bank)
    {
        $bank->delete();
        return redirect()->route('banks.index');
    }

    public function select(Subject $subject){

        $questions = Question::where('subject_id', $subject->id)->get();

        $msg= '';
        foreach ($questions as $question ){
            $msg.='<tr>
                         <td>'.$question->summernote->instructions.'</td>
                         <td>'.$question->mark.'</td>
                         <td>'.$question->negative.'</td>
                         <td>'.$question->subject->name.'</td>
                         
                         <td><input type="radio" name="question_id" value="'.$question->id.'"></td>
                    </tr>';
        }

        return response()->json(array('msg'=> $msg), 200);
    }

    public function selectWhileCreate(Subject $subject, Request $request){
        $arr = $request->request->all();
        if (Bank::where('name', $arr['name'])->exists()){
            $bank = Bank::where('name', $arr['name'])->first();
            $questions_ids =  unserialize($bank->question_ids);
            $questions = Question::where('subject_id', $subject->id)->WhereNotIn('id', $questions_ids)->get();
        }else{
            $questions = Question::where('subject_id', $subject->id)->get();
        }

        $msg= '';
        foreach ($questions as $question ){
            $msg.='<tr>
                         <td>'.$question->summernote->instructions.'</td>
                         <td>'.$question->mark.'</td>
                         <td>'.$question->negative.'</td>
                         <td>'.$question->subject->name.'</td>
                         
                         <td><input type="radio" name="question_id" value="'.$question->id.'"></td>
                    </tr>';
        }

        return response()->json(array('msg'=> $msg), 200);
    }

    public function setWhileCreate(Question $question, Request $request){
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->passes()) {
            $arr = $request->request->all();

            if (Bank::where('name', $arr['name'])->exists()){
                $existing_question_bank = Bank::where('name', $arr['name'])->get()[0];
                $existing_question_bank_question_ids = $existing_question_bank->question_ids;
                $question_ids = unserialize($existing_question_bank_question_ids);
                array_push($question_ids, $question->id);
                $existing_question_bank->question_ids = serialize($question_ids);
                $existing_question_bank->save();
            }else {
                Bank::firstOrCreate(['name' => $arr['name']], ['question_ids' => serialize([$question->id])]);
            }

            $bank = Bank::where('name', $arr['name'])->first();
            $questions_ids =  unserialize($bank->question_ids);
            $list = '';
            if (count($questions_ids) != 0){
                $questions = Question::whereIn('id', $questions_ids)->get();
                foreach ($questions as $question){
                    $list.='<tr>
                            <td>'.$question->summernote->instructions.'</td>

                            <td>'.$question->mark.'</td>
                            <td>'.$question->negative.'</td>
                            <td>'.$question->subject->name.'</td>
                            
                            <td>
                               <div class="row">
                                    <div class="col-sm-3">
                                         <button value="'.$question->id.'" class="unset btn btn-danger">Unset</button>
                                    </div>
                                </div>
                            </td>
                        </tr>';
                }
            }else{
                $list = 'No question assigned yet!';
            }
            return response()->json(array('list'=> $list, 'message'=> ''), 200);
        }

        return response()->json(array('list'=> '', 'message' => ' Add a unique name'), 200);
    }

    public function unset($id, Request $request){
        $arr = $request->request->all();

        $bank = Bank::find($arr['bank_id']);

        $questions_ids = unserialize($bank->question_ids);
        $questions_ids=array_diff($questions_ids, [$id]);


        $msg = '';
        if (count($questions_ids) != 0){
            $questions = Question::whereIn('id', $questions_ids)->get();
            foreach ($questions as $question){
                $msg.='<tr>
                            <td>'.$question->summernote->instructions.'</td>

                            <td>'.$question->mark.'</td>
                            <td>'.$question->negative.'</td>
                            <td>'.$question->subject->name.'</td>
                            
                            <td>
                               <div class="row">
                                    <div class="col-sm-3">
                                         <button value="'.$question->id.'" class="unset btn btn-danger">Unset</button>
                                    </div>
                                </div>
                            </td>
                        </tr>';
            }
        }else{
            $msg = 'No question assigned yet!';
        }

        $bank->question_ids = serialize($questions_ids);
        $bank->save();

        return response()->json(array('message'=> $msg), 200);
    }

    public function unsetWhileCreate(Question $question, Request $request){
        $arr = $request->request->all();
        $bank = Bank::where('name', $arr['name'])->first();

        $questions_ids = unserialize($bank->question_ids);
        $questions_ids = array_diff($questions_ids, [$question->id]);

        $msg = '';
        if (count($questions_ids) != 0){
            $questions = Question::whereIn('id', $questions_ids)->get();
            foreach ($questions as $question){
                $msg.='<tr>
                            <td>'.$question->summernote->instructions.'</td>

                            <td>'.$question->mark.'</td>
                            <td>'.$question->negative.'</td>
                            <td>'.$question->subject->name.'</td>
                            
                            <td>
                               <div class="row">
                                    <div class="col-sm-3">
                                         <button value="'.$question->id.'" class="unset btn btn-danger">Unset</button>
                                    </div>
                                </div>
                            </td>
                        </tr>';
            }
        }else{
            $msg = 'No question assigned yet!';
        }

        $bank->question_ids = serialize($questions_ids);
        $bank->save();

        return response()->json(array('message'=> $msg), 200);
    }

    public function setWhileEdit(Bank $bank, Question $question, Request $request){
        $validator = \Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->passes()) {
            $arr = $request->request->all();
            $bank->name = $arr['name'];
            $bank_question_ids = $bank->question_ids;
            $question_ids = unserialize($bank_question_ids);
            array_push($question_ids, $question->id);
            $bank->question_ids = serialize($question_ids);
            $bank->save();

            $questions_ids =  unserialize($bank->question_ids);
            $list = '';
            if (count($questions_ids) != 0){
                $questions = Question::whereIn('id', $questions_ids)->get();
                foreach ($questions as $question){
                    $list.='<tr>
                            <td>'.$question->summernote->instructions.'</td>

                            <td>'.$question->mark.'</td>
                            <td>'.$question->negative.'</td>
                            <td>'.$question->subject->name.'</td>
                            
                            <td>
                               <div class="row">
                                    <div class="col-sm-3">
                                         <button value="'.$question->id.'" class="unset btn btn-danger">Unset</button>
                                    </div>
                                </div>
                            </td>
                        </tr>';
                }
            }else{
                $list = 'No question assigned yet!';
            }
            return response()->json(array('list'=> $list, 'message'=> '', 'name'=> $bank->name), 200);
        }

        return response()->json(array('list'=> '', 'message' => ' Add a unique name'), 200);
    }

    public function rename(Bank $bank, Request $request ){
        $request->validate([
            'name' => 'required|unique:banks|max:255',
        ]);
        $bank->update(['name'=>$request->name]);
        return back();
    }

    public function selectWhileEdit(Bank $bank,Subject $subject){

        $questions = Question::where('subject_id', $subject->id)->whereNotIn('id', unserialize($bank->question_ids))->get();

        $msg= '';
        foreach ($questions as $question ){
            $msg.='<tr>
                         <td>'.$question->summernote->instructions.'</td>

                         <td>'.$question->mark.'</td>
                         <td>'.$question->negative.'</td>
                         <td>'.$question->subject->name.'</td>
                         
                         <td><input type="radio" name="question_id" value="'.$question->id.'"></td>
                    </tr>';
        }

        return response()->json(array('msg'=> $msg), 200);
    }
}
