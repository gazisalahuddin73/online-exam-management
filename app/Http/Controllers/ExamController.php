<?php

namespace App\Http\Controllers;

use App\User;
use App\Exam;
use App\ExamQuestion;
use Illuminate\Http\Request;
use App\Group;
use App\Summernote;
use App\Question;
use App\Subject;
use App\Bank;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exams = Exam::with('group', 'questions')->get();
        return view('exams.index', compact('exams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = Group::all();
        return view('exams.create', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => ['required','string'],
            'date_time' => ['required'],
            'duration' => ['required','integer'],
            'group_id' => ['required'],
            'instructions'=>['string','nullable'],
        ]);

        $ins = $request->instructions === null ?'<p>No instructions</p>':$request->instructions;
        $dom = new \DomDocument();
        
        $dom->loadHtml($ins, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $detail = $dom->saveHTML();

        $summernote = new Summernote;

        $summernote->instructions = $detail;

        $summernote->save();

        Exam::create([
            'name' => $request->name,
            'date_time' => Carbon::parse($request->date_time)->format('Y-m-d\TH:i'),
            'duration' => $request->duration,
            'group_id' => $request->group_id,
            'summernote_id' => $summernote->id,
            'activate_exam' => $request->activate_exam
        ]);
        \Session::flash('msg_success', 'New exam has been created!'); 
        return redirect()->route('exams.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show(Exam $exam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam)
    {
        $groups = Group::all();
        return view('exams.edit', compact('exam', 'groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam $exam)
    {
        $request->validate([
            'name' => ['required','string'],
            'date_time' => ['required'],
            'duration' => ['required','integer'],
            'group_id' => ['required'],
            'instructions'=>['string','nullable'],
        ]);

        $ins = $request->instructions === null ?'<p>No instructions</p>':$request->instructions;
        
        $dom = new \DomDocument();

        $dom->loadHtml($ins, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $detail = $dom->saveHTML();

        $summernote = $exam->load('summernote')->summernote;

        $summernote->instructions = $detail;

        $summernote->save();

        Exam::updateOrCreate(
            [
            'id'=>$exam->id
            ],
            [
            'name' => $request->name,
            'date_time' => Carbon::parse($request->date_time)->format('Y-m-d\TH:i'),
            'duration' => $request->duration,
            'group_id' => $request->group_id,
            'summernote_id' => $summernote->id,
            'activate_exam' => $request->activate_exam
        ]);
        \Session::flash('msg_success', 'The exam has been updated!');
        return redirect()->route('exams.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $exam)
    {
        $exam->delete();
        \Session::flash('msg_success', 'The exam has been deleted!');
        return redirect()->route('exams.index');
    }

    public function activation($id){
      $exam = Exam::find($id);
      if ($exam->activate_exam == 1){
          $exam->activate_exam = 0;
      }else {
          $exam->activate_exam = 1;
      }
      $exam->save();

      return response()->json(array('msg'=> ''), 200);
    }

    public function manage($id){
        $exam = Exam::find($id);
        $group = Group::find($exam->group_id)->title;
        $subjects = Subject::all();
        $questions_ids = ExamQuestion::where('exam_id', $exam->id)->get()->toArray();
        $temp = [];
        if (count($questions_ids) !== 0){
            foreach ($questions_ids as $key){
                $temp[]=$key['question_id'];
            }
            $questions = Question::whereIn('id', $temp)->get();
        }else{
            $questions = '';
        }

        //$options = '<option selected disabled>Select Subject</option>';
        $options = '';
        foreach($subjects as $subject){
                    $options.='<option value='.$subject->id.'>'.$subject->name.'</option>';
         }

        return view('exams.manage', compact('exam', 'group', 'questions', 'options', 'subjects'));
    }

    public function selectQuestionSet(Exam $exam, Request $request){
         $question_bank = Bank::find($request->bank_id);
         $question_ids = $question_bank->question_ids;
         foreach (unserialize($question_ids) as $id){
             ExamQuestion::create(['exam_id'=> $exam->id, 'question_id'=> $id]);
         }
         return redirect()->route('exams.index');
    }

    public function examManageQuestionSave(Request $request){
        $request->validate([
            'body' => 'required',
            'mark' => 'required|integer',
            'negative' => 'required|max:2',
            'correct' => 'required|string',
            'order' => 'integer|max:2',
            'subject_id'=>'required',
            'option_.*'=>'required'  
        ]);
        $computed_array = [];
        foreach ($request->request as $key=>$val){
            $computed_array[$key] = $val;
        }

        $sliced = array_slice($computed_array, 7);

        $options = [];
        
        /*
         * Now this loop does three things at once,
         * it filters through and makes array with
         * question options and correct answers
         */
        foreach($sliced as $key=>$value){
            if ("correct" == substr($key,0,7)){
                $correct_ans=$value;
            }elseif ("option_" == substr($key,0,7)){
                $options[$key] = $value;
            }
        }

        $dom = new \DomDocument();
        $dom->loadHTML('<?xml encoding="UTF-8">'. $request->body);
        $detail = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $detail .= $dom->saveHTML( $dom->documentElement );
        $summernote = new Summernote;
        $summernote->instructions = $detail;
        $summernote->save();
        
        $question = Question::create([
            'body' => $summernote->id,
            'subject_id' => $request->subject_id,
            'mark' => $request->mark,
            'negative' => $request->negative,
            'options' => serialize(array_values($options)),
            'correct' => serialize([intval(str_replace('option_', '', $correct_ans))]),
            'order' => $request->order
        ]);
        ExamQuestion::create(['exam_id'=> $request->exam_id+0, 'question_id'=>$question->id]);
        return redirect()->route('exams.manage', $request->exam_id);
    }

    public function questionList(Request $request, $id=1){
        $subject = Subject::find($id);

        $store_request = $request->request->all();
        $summernote_ids = Summernote::where('instructions', 'LIKE', '%' .$store_request['question_description'].'%')->pluck('id');

        $questions = Question::where('subject_id', $subject->id)->whereIn('body', $summernote_ids)->get();
        $msg = '';
        foreach ($questions as $question){
            if (ExamQuestion::where(['exam_id'=> $store_request['exam_id'],'question_id'=> $question->id])->exists() == false){
                $msg.='<tr>
                         <td>'.$question->summernote->instructions.'</td>

                         <td>'.$question->mark.'</td>
                         <td>'.$question->negative.'</td>
                         <td>'.$question->subject->name.'</td>
                         <td>'.$question->order.'</td>
                         <td><input type="radio" name="question_id" value="'.$question->id.'"></td>
                    </tr>';
            }
        }
        return response()->json(array('message'=> $msg), 200);
    }

    public function enrollStudentFrontend(Request $request){

        if(filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            $type = 'email';
           }else{
            $type = 'regex:/(01)[0-9]{9}/';
           }

        $request->validate([
            'email'   => "required|$type",
            'password' => 'required|min:6'
        ]);

        if(is_numeric($request->get('email'))){
            if (\Auth::guard('web')->attempt(['phone' => $request->get('email'), 'password' => $request->password])) {
                $exam = Exam::where('name', $request->exam)->first();
                $exam_students = DB::table('exam_students')->where('exam_id', $exam->id);
                if($exam_students->exists()){
                    $ids = unserialize($exam_students->first()->students);
                    array_push($ids, \Auth::user()->id);
                    $exam_students->updateOrInsert(
                        ['id'=>$exam_students->first()->id],
                        ['exam_id' => $exam->id, 'students' => serialize($ids)]
                    );
                }else{
                    DB::table('exam_students')->insert(
                        ['exam_id' => $exam->id, 'students' => serialize([\Auth::user()->id])]
                    );
                }
                \Session::flash('msg_success', 'You have been enrolled!');
                return redirect()->intended('/home');
            }
        }
        
        if (\Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password])) {
            $exam = Exam::where('name', $request->exam)->first();
            $exam_students = DB::table('exam_students')->where('exam_id', $exam->id);
            if($exam_students->exists()){
                $ids = unserialize($exam_students->first()->students);
                array_push($ids, \Auth::user()->id);
                $exam_students->updateOrInsert(
                    ['id'=>$exam_students->first()->id],
                    ['exam_id' => $exam->id, 'students' => serialize($ids)]
                );
            }else{
                DB::table('exam_students')->insert(
                    ['exam_id' => $exam->id, 'students' => serialize([\Auth::user()->id])]
                );
            }
            \Session::flash('msg_success', 'You have been enrolled!');    
            return redirect()->intended('/home');
            }
        return back()->withInput($request->only('email', 'remember'));
    }
}
