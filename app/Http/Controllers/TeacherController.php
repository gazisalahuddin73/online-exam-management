<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $teachers = User::whereNull('institution_id')->where('is_admin',0)->get();;
      return view('teachers.index', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $request->validate([
            'name' => 'required|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
            'email' => 'required|email|unique:users',
            'phone' => 'required|regex:/(01)[0-9]{9}/|unique:users',
            'password' => 'required|min:8|confirmed',
        ]);
        $teacher = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password)
        ]);
        $teacher->is_active=1;
        $teacher->save();
        $teacher->assignRole('Teacher');
        $permissions = \Spatie\Permission\Models\Permission::whereNotIn('name', ['manage accessibility', 'do exam'])->get();
        foreach($permissions as $permission){
            \DB::table('model_has_permissions')->insert(['permission_id'=> $permission->id, 'model_type'=> 'App\User', 'model_id'=> $teacher->id]);
        }
        \Session::flash('msg_success', 'New teacher has been created!');
        return redirect()->route('teachers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = User::find($id);
        return view('teachers.edit', compact('id', 'teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required','regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/'],
            'email' => ['required','email',Rule::unique('users')->ignore($id)],
            'phone' => ['required','regex:/(01)[0-9]{9}/',Rule::unique('users')->ignore($id)],
            
        ]);

        User::updateOrCreate([
            'id'=>$id
        ],
        [
            'name' => $request->name,
            'email'=>$request->email,
            'phone'=> $request->phone,
        ]);
        \Session::flash('msg_success', 'Teacher info updated!');
        return redirect()->route('teachers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        \Session::flash('msg_success', 'The teacher has been deleted!');
        return back();
    }

    public function activation(User $teacher){
        if ($teacher->is_active == 1){
            $teacher->removeRole('Teacher');
            \DB::table('model_has_permissions')->where('model_type', 'App\User')->where('model_id', $teacher->id)->delete();
            $teacher->is_active = 0;
        }else {
            $teacher->is_active = 1;
            $teacher->assignRole('Teacher');
            $permissions = \Spatie\Permission\Models\Permission::whereNotIn('name', ['manage accessibility', 'do exam'])->get();
            foreach($permissions as $permission){
                //$teacher->givePermissionTo($permission->name);
                \DB::table('model_has_permissions')->insert(['permission_id'=> $permission->id, 'model_type'=> 'App\User', 'model_id'=> $teacher->id]);
            }
        }
        $teacher->save();
    }
}
