<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Result;
use App\Exam;

class ResultController extends Controller
{
    public function index(){
        $results = Result::groupBy('exam_id')
            ->join('exams', 'results.exam_id','=', 'exams.id')
            ->select(\DB::raw('count(student_id) as students'), 'exams.name', 'exam_id')->get();

        return view('results.index', compact('results'));
    }

    public function resultFromTeachersPanel(Exam $exam){
        $results = Result::where('exam_id', $exam->id)->get();
        return view('teachers.resultFromTeachersPanel', compact('results'));
    }

    public function resultSheetTeachersPanel( Result $result, Exam $exam, User $user){
     $questions = unserialize($result->questions);
     $right = unserialize($result->right);
     $wrong = unserialize($result->wrong);
     $no_response = unserialize($result->no_response);
     $original_responses = unserialize($result->original_response);

     return view('teachers.resultsheet', compact('questions', 'right', 'wrong', 'no_response', 'original_responses'));
    }


}
