<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Spatie\Permission\Models\Role; 

class AccessibilityController extends Controller
{
    public function index(){ 
      //make this query this way so that teacher/student related perimission gets selected 
      $roles = Role::all();
      $role_has_permissions = \DB::table('role_has_permissions')->get();
      $role_has_permissions = $role_has_permissions->groupBy('role_id');

      $final = [];
      foreach($roles as $role){
        if(!array_key_exists($role->id, $role_has_permissions->toArray())){
          $final[] = [$role->name => []];
        }else{
          $final[] = [$role->name => [$role_has_permissions[$role->id]]];
        }
      }

      return view('accessibility.accessibility', compact('final'));
    }

    public function edit(Role $role){
      if($role->id==3){
        $permissions = \DB::table('permissions')->where('guard_name','=','web')->pluck('id');
      }elseif($role->id==2){
        $permissions = \DB::table('permissions')->where('guard_name', 'web')->pluck('id');
      }elseif($role->id==1){
        $permissions = \DB::table('permissions')->where('guard_name', 'web')->pluck('id');
      }
      
      return view('accessibility.edit', compact('role', 'permissions'));
    }

    public function update(Role $role, Request $request){
      \DB::table('role_has_permissions')->where('role_id', $role->id)->delete();
      $user_ids = \DB::table('model_has_roles')->where('role_id', $role->id)->pluck('model_id');
      \DB::table('model_has_permissions')->whereIn('model_id', $user_ids)->delete();
      foreach($request->permission_ids as $id){
        \DB::table('role_has_permissions')->insert(['role_id'=> $role->id, 'permission_id'=>$id]);
        foreach($user_ids as $user_id){
          \DB::table('model_has_permissions')->insert(['permission_id'=>$id, 'model_type'=>'App\User', 'model_id'=>$user_id]);
        }
      }
      return response()->json(['data'=>'']);
    }

    public function addPermission(){
      return view('accessibility.add-permission');
    }

    public function savePermission(Request $request){
      $request->validate([
        'name' => ['required', 'unique:permissions', 'max:255'],
      ]);

      \Spatie\Permission\Models\Permission::create(['name'=>$request->name, 'guard_name'=>'web']);
    
      return back();
    }

    public function permissions(){
      $permissions = \Spatie\Permission\Models\Permission::paginate(5);
      return view('accessibility.permissions', compact('permissions'));
    }

    public function editPermission($id){
      $permission = \Spatie\Permission\Models\Permission::find($id);
      return view('accessibility.permission-edit', compact('permission'));
    }

    public function updatePermission($id, Request $request){
      $request->validate([
        'name' => ['required', 'unique:permissions', 'max:255']
      ]);

      \Spatie\Permission\Models\Permission::updateOrCreate([
        'id'=>$id
      ],[
         'name'=> $request->name,
         'guard_name'=> 'web'
      ]);
      
      return redirect()->route('permissions');
    }

    public function deletePermission($id){
      \Spatie\Permission\Models\Permission::find($id)->delete();
      return redirect()->route('permissions');
    }
}
