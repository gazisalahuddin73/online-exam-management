<?php

namespace App\Http\Controllers;

use App\Competition;
use App\Exam;
use App\ExamQuestion;
use App\Question;
use App\User;
use App\Institution;
use App\ExamStudent;
use App\Result;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class StudentController extends Controller
{
    public function index(){
        $students = User::whereNotNull('institution_id')->with('institution')->get();
        return view('students.index', compact('students'));
    }

    public function approval(User $student){
       
        if($student->is_active == 0){
            $student->is_active=1;
            $student->assignRole('Student');
            $permission = \Spatie\Permission\Models\Permission::find(10);
            $student->givePermissionTo($permission->name);
        }else{
            $student->is_active=0;
            $student->removeRole('Student');
            \DB::table('model_has_permissions')->where('model_type', 'App\User')->where('model_id', $student->id)->delete();
            } 
        $student->save();
        $message = $student->is_active == 0 ? 'Disapprove' : 'Approve';
        return response()->json(array('msg'=> $message), 200);
    }

    public function create(){
        $institutions = Institution::all();
        return view('students.create', compact('institutions'));
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'institution_id' => 'required|max:250',
            'class' => 'required',
            'roll_no' => 'required|integer',
            'phone' => 'required|regex:/[0-9]{9}/',
            'email' => 'nullable|email',
            'password' =>'required'
        ]);

        User::firstOrCreate(['phone'=>$request->phone, 'email'=>$request->email], [
            'name'=> $request->name,
            'institution_id'=> $request-> institution_id,
            'class'=> $request->class,
            'roll_no'=> $request->roll_no,
            'password'=> \Hash::make($request->password),
            'is_admin'=>0
        ]);
        \Session::flash('msg_success', 'New student has been created!');
        return redirect()->route('students.index');
    }

    public function addExamForm(){
        $exams = Exam::all();
        $institutions = Institution::all();
        return view('students.add_to_exams', compact('exams', 'institutions'));
    }

    public function studentListFromInstitution(Request $request, Institution $institution){

      if(!ExamStudent::where('exam_id', $request->exam_id)->exists()){
          $students = User::where('institution_id', $institution->id)
              ->where('class', $request->class)
              ->get();
      }else{
          $exam_student = ExamStudent::where('exam_id', $request->exam_id)->first();
          $students = User::where('institution_id', $institution->id)
              ->where('class', $request->class)
              ->whereNotIn('id', unserialize($exam_student->students))
              ->get();
      }

      $temp = '';
      foreach ($students as $student){
          $temp.='<tr>'.'<td>'.$student->name.'</td><td>'.$student->roll_no.'</td><td><input type="checkbox"><input type="hidden" name="student_id" value='.$student->id.'></td></tr>';
      }

      return response()->json(array('msg'=> $temp), 200);
    }

    public function attachToExam($id, Request $request){
       $exam_student = ExamStudent::where('exam_id', $request->exam_id)->exists();//dd(gettype($exam_student));
       if ($exam_student){
          $exam_student =  ExamStudent::where('exam_id', $request->exam_id)->first();
          $student_ids = unserialize($exam_student->students);
          array_push($student_ids, $id);
          $pushed = array_unique($student_ids);
       }else{
           $pushed = [$id];
       }

       ExamStudent::updateOrCreate([
            'exam_id' => $request->exam_id
        ],[
            'students' => serialize($pushed)
        ]);
        $student = User::find($id);
        $std= '<tr>
                     <td>'.$student->name.'</td>
                     <td>'.$student->institution->name.'</td>
                     <td>'.$student->class.'</td>
                     <td>'.$student->roll_no.'</td>
                     <td><a href="http://127.0.0.1:8000/student/unset/from/list/'.$request->exam_id.'/'.$student->id.'"><button class="btn-primary">Unset</button></a></td>
               </tr>';
        return response()->json(array('msg'=> $std, 200));
    }

    public function examStudents(){
        $exam_students = ExamStudent::with('exam')->get();
        return view('students.exam_students', compact('exam_students'));
    }

    public function studentUnsetFromList(Exam $exam, User $student){
        $exam_student = ExamStudent::where('exam_id', $exam->id)->first();

        $unserialized = unserialize($exam_student->students);
        $without_provided_student_id = array_diff($unserialized, array($student->id));
        $pushed = array_unique($without_provided_student_id);

        ExamStudent::updateOrCreate([
            'exam_id' => $exam->id
        ],[
            'students' => serialize($pushed)
        ]);

        return back();

    }

    public function examStudentEditForm(ExamStudent $examStudent){
        $institutions = \App\Institution::all();
        return view('students.examStudentEditForm', compact('examStudent', 'institutions'));
    }

    public function examStudentsDelete(ExamStudent $examStudent){
        $examStudent->delete();
        return back();
    }

    public function getProfile(){
        $student = Auth::user();
        $exam_students = ExamStudent::all();

        return view('students.profile', compact('student', 'exam_students'));
    }

    public function examAttend(Request $request, Exam $exam, $id){
        $student = User::find($id);
        $question_ids = ExamQuestion::where('exam_id', $exam->id)->pluck('question_id');
        $questions = '';
        foreach ($question_ids as $question_id){
            $options = unserialize(Question::find($question_id)->options);
            $answers = '';
            $letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'];
            for ($i=0; $i<count($options); $i++){
                $answers.=$letters[$i].':"'.$options[$i].'",';
            }
            $correct = unserialize(Question::find($question_id)->correct);
            $questions.='{
                question:"'.strip_tags(Question::find($question_id)->summernote->instructions).'",

                answers: {
                        '.$answers.'
                },
                question_id:{
                  id: '.$question_id.'
                },
                correctAnswer: "'.$letters[$correct[0]].'"
            },';
        }
        $exam_start = Carbon::parse($exam->date_time)->subHours(6)->toIso8601String();
        $exam_end = Carbon::parse($exam->date_time)->addMinutes($request->duration)->toIso8601String();
        return view('students.mcq-paper', compact('exam_start','exam_end','exam', 'student', 'questions'));
    }

    public function resultCreate(Exam $exam, $id, Request $request){
        $right = [];
        $wrong = [];
        $response = [];
        $letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
        foreach ($request->checkedOptions as $checkedOption) {
           $response[] = $checkedOption[0]+0;
           $correct_options = unserialize(Question::find($checkedOption[0]+0)->correct);
           if ($correct_options[0] == array_search($checkedOption[1], $letters)){
            $right[] = $checkedOption[0]+0;
           }
        }

        $no_response = array_diff($request->questionIDs, $response);
        $wrong = array_diff($request->questionIDs, array_merge($right, $no_response));
        Result::firstOrCreate([
            'exam_id' => $exam->id,
            'student_id' => $id
        ],[
            'questions' => serialize($request->questionIDs),
            'right' => serialize($right),
            'wrong' => serialize($wrong),
            'no_response' => serialize($no_response),
            'original_response' => serialize($request->checkedOptions)
        ]);

        return response()->json(['url'=>url('/student')]);
    }

    public function resultsList(){
      $student = Auth::user();
      $results = Result::where('student_id', $student->id)->get();
      return view('students.results-list', compact('results', 'student'));
    }

    public function resultSheetStudentPanel( Result $result, Exam $exam, User $user){
        $questions = unserialize($result->questions);
        $right = unserialize($result->right);
        $wrong = unserialize($result->wrong);
        $no_response = unserialize($result->no_response);
        $original_responses = unserialize($result->original_response);

        return view('students.result-sheet', compact('questions', 'right', 'wrong', 'no_response', 'original_responses'));
    }

    public function competitions(){
        //first check at-least one exam of a competition is still awaiting for completion/happening
        //this student has been included in one of the exams
        $exam_list = [];
        $competitions = Competition::all();
        foreach ($competitions as $competition){
            $student_ids = unserialize($competition->student_ids);
            //here need to ensure any exam is valid this time with carbon now
            $exam_ids = unserialize($competition->exam_ids);
            if(in_array(Auth::user()->id, $student_ids)){
                $exam_list[]=['title'=> $competition->title, 'exam_ids'=> $exam_ids];
            }
        }
        return view('students.competition', compact('exam_list'));
    }
}
