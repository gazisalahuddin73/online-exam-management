<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        if(filter_var($request->email, FILTER_VALIDATE_EMAIL)){
         $type = 'email';
        }else{
         $type = 'regex:/(01)[0-9]{9}/';
        }
        $this->validate($request, [
                     'email'   => "required|$type",
                     'password' => 'required|min:6'
                 ]);
        
        if(is_numeric($request->get('email'))){
            if (\Auth::guard('web')->attempt(['phone' => $request->get('email'), 'password' => $request->password], $request->get('remember'))) {
                return redirect()->intended('/home');
            }
        }
        $request->only($this->username(), 'password');
        
        if (\Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
                     return redirect()->intended('/home');
                 }
        return back()->withInput($request->only('email', 'remember'));
    }
}
