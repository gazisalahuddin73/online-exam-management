<?php

namespace App\Http\Controllers;

use App\Summernote;
use Illuminate\Http\Request;

class SummernoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Summernote  $summernote
     * @return \Illuminate\Http\Response
     */
    public function show(Summernote $summernote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Summernote  $summernote
     * @return \Illuminate\Http\Response
     */
    public function edit(Summernote $summernote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Summernote  $summernote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Summernote $summernote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Summernote  $summernote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Summernote $summernote)
    {
        //
    }

    public function getSummernoteeditor()
    {
        return view('summernoteeditor');

    }



    /**







     *



     * @return \Illuminate\Http\Response



     */



    public function postSummernoteeditor(Request $request)
    {

        $request->validate($request, [

            'detail' => 'required',

            'feature' => 'required'

        ]);



        $detail=$request->input('detail');

        $feature=$request->input('feature');



        $dom = new \DomDocument();



        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);


        $detail = $dom->saveHTML();

        $summernote = new Summernote;

        $summernote->content = $detail;

        $summernote->Feature=$feature;

        $summernote->save();


        echo "<h1>Feature</h1>" , $feature;


        echo "<h2>Details</h2>" , $detail;

        return redirect('home');

    }

}
