<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth:web','role:Admin|Teacher'])->group(function() {
    Route::resource('/groups', GroupController::class);
    Route::resource('/exams', ExamController::class);
    Route::get('/students', 'StudentController@index')->name('students.index');
    Route::get('/student/create' , 'StudentController@create')->name('student.create');
    Route::post('student/approval/{student}', 'StudentController@approval');
    Route::post('student/store', 'StudentController@store')->name('student.store');

    Route::post('/exams/activation/{id}', 'ExamController@activation')->name('exams.activation');
    Route::get('/exams/manage/{id}', 'ExamController@manage')->name('exams.manage');
    Route::post('/exams/manage/question/list/{id?}', 'ExamController@questionList')->name('exams.manage.question.list');
    Route::post('/exams/assign-question', 'ExamController@assignQuestion')->name('exams.assignQuestion');

    Route::get('/question/create', 'QuestionController@create')->name('question.create');
    Route::post('/question/save', 'QuestionController@save')->name('question.save');
    Route::post('question/unset/{id}', 'QuestionController@unset')->name('question.unset');
    Route::get('/question/{id}/edit', 'QuestionController@edit')->name('question.edit');
    Route::post('/question/update/{question}', 'QuestionController@update')->name('question.update');
    Route::get('/question/select/{subject}', 'QuestionController@select')->name('question.select');
    Route::post('/question/set/{question}', 'QuestionController@set')->name('question.set');
    Route::delete('/question/destroy/{question}', 'QuestionController@destroy')->name('question.destroy');

    Route::get('/questions', 'QuestionController@index')->name('questions.index');
    Route::post('/select/question/set/{exam}', 'ExamController@selectQuestionSet')->name('select.question.set');

    Route::resource('/banks', BanksController::class);
    Route::get('/banks/question/select/{subject}', 'BanksController@select')->name('banks.question.select');
    Route::post('banks/question/unset/{id}', 'BanksController@unset');
    Route::post('/banks/question/unsetWhileCreate/{question}', 'BanksController@unsetWhileCreate');
    Route::post('banks/question/set/while/edit/{bank}/{question}', 'BanksController@setWhileEdit');
    Route::post('banks/question/set/while/create/{question}', 'BanksController@setWhileCreate');
    Route::get('/banks/question/select/while/create/{subject}', 'BanksController@selectWhileCreate');

    Route::post('exam/manage/question/save/', 'ExamController@examManageQuestionSave')->name('exam.manage.question.save');
    Route::post('bank/rename/{bank}', 'BanksController@rename');
    Route::get('/banks/question/select/while/edit/{bank}/{subject}', 'BanksController@selectWhileEdit');

    Route::get('/exam-students', 'StudentController@examStudents');
    Route::get('/exam-students/delete/{examStudent}', 'StudentController@examStudentsDelete');
    Route::get('/exam-students/edit/form/{examStudent}', 'StudentController@examStudentEditForm')->name('student-exam.edit.form');
    Route::get('/student/unset/from/list/{exam}/{student}', 'StudentController@studentUnsetFromList')->name('student.unset.from.list');
    Route::get('/student/add/to/exams', 'StudentController@addExamForm');
    Route::post('/student/list/from/institution/{institution}', 'StudentController@studentListFromInstitution');
    Route::post('/student/attach/to/exam/{id}', 'StudentController@attachToExam');

    Route::get('/results', 'ResultController@index');
    Route::get('/result/from/teachers/panel/{exam}', 'ResultController@resultFromTeachersPanel')->name('result.teacher');
    Route::get('/result/sheet/from/teachers/panel/{result}/{exam}/{user}', 'ResultController@resultSheetTeachersPanel')->name('result.sheet');

    Route::get('/competitions', 'CompetitionController@index');
    Route::get('/competitions/create', 'CompetitionController@create')->name('competitions.create');
    Route::post('/competitions/store', 'CompetitionController@store')->name('competitions.store');
    Route::get('/competitions/{competition}', 'CompetitionController@show')->name('competition.show');
    Route::get('/competitions/edit/{competition}', 'CompetitionController@edit')->name('competition.edit');
    Route::post('/competitions/update/{competition}', 'CompetitionController@update')->name('competition.update');
    Route::get('/competitions/delete/{competition}', 'CompetitionController@delete')->name('competition.delete');
    //Route::post('competitions/student/list/from/institution/{institution}', 'CompetitionController@studentList');
    Route::resource('/institutions', InstitutionController::class);
    Route::resource('/subjects', SubjectController::class);
});

Route::get('/register/students', 'Auth\RegisterController@showStudentRegisterForm');
Route::post('/register/students', 'Auth\RegisterController@createStudent');

Route::middleware(['auth:web','role:Admin'])->group(function() {
    Route::view('/admin', 'admin');
    Route::resource('teachers', TeacherController::class);
    Route::post('/teacher/activation/{teacher}', 'TeacherController@activation');
    Route::get('accessibility', 'AccessibilityController@index')->name('accessibility');
    Route::get('accessibility/edit/{role}', 'AccessibilityController@edit')->name('accessibility.edit');
    Route::post('accessibility/update/{role}', 'AccessibilityController@update')->name('accessibility.update');
    Route::get('permissions', 'AccessibilityController@permissions')->name('permissions');
    Route::get('add/permission', 'AccessibilityController@addPermission')->name('add.permission');
    Route::get('save/permission', 'AccessibilityController@savePermission')->name('save.permission');
    Route::get('permission/edit/{id}', 'AccessibilityController@editPermission')->name('edit.permission');
    Route::get('permission/update/{id}', 'AccessibilityController@updatePermission')->name('update.permission');
    Route::get('permission/delete/{id}', 'AccessibilityController@deletePermission')->name('delete.permission');
});

Route::middleware(['auth:web','role:Teacher|Admin|Student'])->group(function() {
    Route::get('/profile/edit', 'ProfileController@edit')->name('profile.edit');
    Route::get('/profile/update/{role}/{id}', 'ProfileController@update')->name('profile.update');
});

Route::middleware(['auth:web','role:Student'])->group(function () {
    Route::get('/student', 'StudentController@getProfile')->name('profile');
    Route::get('/student/exam/attend/{exam}/{id}', 'StudentController@examAttend')->name('exam.attend');
    Route::post('result/create/{exam}/{id}', 'StudentController@resultCreate');
    Route::get('/student/results', 'StudentController@resultsList')->name('student.results');
    Route::get('/student/result/sheet/{result}/{exam}/{user}', 'StudentController@resultSheetStudentPanel')->name('student.result.sheet');
    Route::get('/student/competitions', 'StudentController@competitions');
});

//Exam enrollment
Route::get('enroll/exam/{examName}', function () {
    auth()->logout();
    return view('exams/enroll');
});
Route::post('enroll/student/frontend', 'ExamController@enrollStudentFrontend')->name('enroll.student.frontend');

//Competition enrollment
Route::get('enroll/competition/{competitionName}', function () {
    auth()->logout();
    return view('competitions/enroll');
});
Route::post('enroll/student/frontend/competition', 'CompetitionController@enrollStudentFrontend')->name('enroll.student.frontend.competition');
